package com.muslimtekno.bigsales;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.muslimtekno.bigsales.app.MyAppController;
import com.muslimtekno.bigsales.config.GlobalConfig;
import com.muslimtekno.bigsales.listAdapter.ListAdapaterPiutang;
import com.muslimtekno.bigsales.model.Piutang;
import com.muslimtekno.bigsales.mysp.ObscuredSharedPreferences;
import com.muslimtekno.bigsales.util.MyLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/**
 * Created by imamudin on 22/03/17.
 */
public class DaftarPiutang extends AppCompatActivity {

    List<Piutang> list_datas = new ArrayList<Piutang>();
    ListView lv_kota;
    ListAdapaterPiutang adapter;
    RequestQueue requestQueue = null;

    ObscuredSharedPreferences pref;
    JsonObjectRequest request =null;
    private SwipeRefreshLayout swipeContainer;
    LinearLayout ll_main, ll_filter;

    int offSet=0;
    Handler handler;
    Runnable runnable;
    Boolean disableSwipeDown = false;       //untuk mendisable swipe down list view
    ProgressDialog loading;

    MyLog myLog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history_kunjungan);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Daftar Piutang");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(false);

        pref = new ObscuredSharedPreferences(this,
                this.getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );
        myLog = new MyLog(getLocalClassName());

        ll_main     =   (LinearLayout)findViewById(R.id.ll_main);
        ll_filter   =   (LinearLayout)findViewById(R.id.ll_filter);


        lv_kota = (ListView)findViewById(R.id.custom_list);
        list_datas.clear();
        adapter = new ListAdapaterPiutang(DaftarPiutang.this, list_datas);
        lv_kota.setAdapter(adapter);

        swipeContainer = (SwipeRefreshLayout)findViewById(R.id.swipeContainer);

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                calllist_datas(0);
                Toast.makeText(DaftarPiutang.this,"refresh",Toast.LENGTH_LONG).show();
            }
        });
        swipeContainer.post(new Runnable() {
            @Override
            public void run() {
                swipeContainer.setRefreshing(true);
                calllist_datas(0);
            }
        });
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        lv_kota.setOnScrollListener(new AbsListView.OnScrollListener() {

            private int currentVisibleItemCount;
            private int currentScrollState;
            private int currentFirstVisibleItem;
            private int totalItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                this.currentScrollState = scrollState;
                this.isScrollCompleted();
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                this.currentFirstVisibleItem = firstVisibleItem;
                this.currentVisibleItemCount = visibleItemCount;
                this.totalItem = totalItemCount;
            }
            private void isScrollCompleted() {
                if (totalItem - currentFirstVisibleItem == currentVisibleItemCount
                        && this.currentScrollState == SCROLL_STATE_IDLE) {
                    if(!disableSwipeDown) {
                        swipeContainer.setRefreshing(true);
                        handler = new Handler();

                        runnable = new Runnable() {
                            public void run() {
                                calllist_datas(offSet);
                            }
                        };
                        //untuk menerlambatkan 0 detik
                        handler.postDelayed(runnable, 000);
                    }else{
                        //Toast.makeText(DaftarPiutang.this,"Data telah ditampilkan semua.",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        ll_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent filter = new Intent(DaftarPiutang.this, Filter.class);
                startActivityForResult(filter, GlobalConfig.KODE_INTENT_FILTER_KUNJUNGAN);
            }
        });
        lv_kota.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object o = lv_kota.getItemAtPosition(position);
                Piutang piutang = (Piutang) o;

                getDetailPiutang(piutang.NAMA,piutang.KODE);
            }
        });
    }
    private void getDetailPiutang(final String nama, String KODE){
        String url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_DETAIL_PIUTANG;
        myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(),""+url);

        loading = ProgressDialog.show(DaftarPiutang.this, "", "Mohon tunggu...", true);
        //bisa order
        JSONObject jsonBody;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(GlobalConfig.USER_ID, pref.getString(GlobalConfig.USER_ID, ""));
            jsonBody.put(GlobalConfig.USER_TOKEN, pref.getString(GlobalConfig.USER_TOKEN, ""));

            jsonBody.put(GlobalConfig.GP_KODE, KODE);
            myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), jsonBody.toString());
            request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loading.dismiss();
                    myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), response.toString());
                    try {
                        int status      = response.getInt("status");
                        String message  = response.getString("message");
                        if(status==1){
                            JSONObject datas = response.getJSONObject("data");
                            showDetailPiutang(nama, datas.toString());
                        }else{
                            notifikasi(message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //myLog.print("respons",response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loading.dismiss();
                    NetworkResponse response = error.networkResponse;
                    if(response != null && response.data != null){
                        myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "code"+response.statusCode);
                        switch(response.statusCode){
                            case 404:
                                notifikasi("Terjadi masalah dengan server.");
                                break;
                            case 408:
                                notifikasi("Waktu terlalu lama untuk memproses, silakan ulangi lagi!");
                                break;
                            case 500:
                                notifikasi("Terjadi masalah dengan server.");
                                break;
                            default:
                                notifikasi("Mohon maaf terjadi kesalahan.");
                                break;
                        }
                    }
                }
            }){
                public Map<String, String> getHeaders() {
                    Map<String,String> headers = new Hashtable<String, String>();

                    //Adding parameters
                    headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }};

            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MyAppController.getInstance().addToRequestQueue(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void showDetailPiutang(String title, String dataString){
        LayoutInflater layoutInflater = (LayoutInflater)getLayoutInflater();
        View promptView = layoutInflater.inflate(R.layout.notif_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DaftarPiutang.this);
        alertDialogBuilder.setView(promptView);

        final TextView t_title      = (TextView) promptView.findViewById(R.id.t_title_dialog);
        final LinearLayout ll_notif = (LinearLayout) promptView.findViewById(R.id.ll_notif);

        t_title.setText(title);
        try {
            //myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), dataString);
            JSONObject data = new JSONObject(dataString);
            JSONArray datas = data.getJSONArray("piutang");
            for(int i=0;i<datas.length();i++){
                JSONObject piutang  = datas.getJSONObject(i);

                String faktur       = piutang.getString(GlobalConfig.GDP_NO_FAKTUR);
                String tanggal_faktur= piutang.getString(GlobalConfig.GDP_TGL_FAKTUR);
                String total        = piutang.getString(GlobalConfig.GDP_TOTAL);
                String jatuh_tempo  = piutang.getString(GlobalConfig.GDP_JATUH_TEMPO);
                String belum_dibayar= piutang.getString(GlobalConfig.GDP_BELUM_DIBAYAR);

                if(total.length()<=0) total ="0";

                ll_notif.addView(getViewDataPiutang(faktur, tanggal_faktur, decimalToRupiah(Double.parseDouble(total)), jatuh_tempo, decimalToRupiah(Double.parseDouble(belum_dibayar))));
            }
        }catch (Exception e){
            //myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), e.getMessage().toString());
            myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "Tidak dapat memproses data json.");
        }
        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setNegativeButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
    private View getViewDataPiutang(String faktur, String tanggal_faktur, String total, String jatuh_tempo, String belumdibayar){
        LayoutInflater inflater     = (LayoutInflater)this.getLayoutInflater();
        View data_history_view    = inflater.inflate(R.layout.list_piutang_dialog, null);

        final TextView tv_faktur        =(TextView)data_history_view.findViewById(R.id.tv_faktur);
        final TextView tv_tanggal_faktur=(TextView)data_history_view.findViewById(R.id.tv_tanggal_faktur);
        final TextView tv_total         =(TextView)data_history_view.findViewById(R.id.tv_total);
        final TextView tv_jatuh_tempo   =(TextView)data_history_view.findViewById(R.id.tv_tanggal_jatuh_tempo);
        final TextView tv_belum_dibayar =(TextView)data_history_view.findViewById(R.id.tv_belum_dibayar);

        tv_faktur.setText(faktur);
        tv_tanggal_faktur.setText(tanggal_faktur);
        tv_total.setText(total);
        tv_jatuh_tempo.setText(jatuh_tempo);
        tv_belum_dibayar.setText(belumdibayar);

        return data_history_view;
    }
    private String decimalToRupiah(double harga){
        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        kursIndonesia.setDecimalFormatSymbols(formatRp);

        String hasil = kursIndonesia.format(harga);
        return hasil.substring(0, (hasil.length()-3));
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case GlobalConfig.KODE_INTENT_FILTER_KUNJUNGAN:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        calllist_datas(0);
                        break;
                }
                break;
        }
    }
    private void calllist_datas(int page){
        if(page==0){
            list_datas.clear();
            adapter.notifyDataSetChanged();
            offSet=0;
            disableSwipeDown = false;
        }
        swipeContainer.setRefreshing(true);
        requestQueue = Volley.newRequestQueue(this);

        // Creating volley request obj
        String url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_PIUTANGS;

        myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), url);
        JSONObject jsonBody;
        jsonBody = new JSONObject();
        try {
            jsonBody.put(GlobalConfig.USER_ID, pref.getString(GlobalConfig.USER_ID, ""));
            jsonBody.put(GlobalConfig.USER_TOKEN, pref.getString(GlobalConfig.USER_TOKEN, ""));

            jsonBody.put(GlobalConfig.UP_KODE, pref.getString(GlobalConfig.FILTER_TOKOID, ""));

            jsonBody.put(GlobalConfig.UP_TGL_AWAL, pref.getString(GlobalConfig.FILTER_TGL_MULAI, ""));
            jsonBody.put(GlobalConfig.UP_TGL_AKHIR, pref.getString(GlobalConfig.FILTER_TGL_AKHIR, ""));

            jsonBody.put(GlobalConfig.UP_START, page);
            jsonBody.put(GlobalConfig.UP_LIMIT, GlobalConfig.MAX_ROW_PER_REQUEST);

            myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), jsonBody.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    swipeContainer.setRefreshing(false);
                    myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), response.toString());
                    try {
                        int status      = response.getInt("status");
                        String message  = response.getString("message");
                        if(status==1){
                            JSONObject respon_data  = response.getJSONObject("data");
                            int total               = respon_data.getInt("total");

                            if(total<GlobalConfig.MAX_ROW_PER_REQUEST || total==0) {
                                disableSwipeDown = true;
                            }
                            offSet += total;
                            JSONArray datas = respon_data.getJSONArray("piutang");
                            if(datas.length()>0) {
                                for (int i = 0; i < datas.length(); i++) {
                                    JSONObject data = datas.getJSONObject(i);
                                    myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), data.toString());

                                    Piutang piutang = new Piutang();
                                    piutang.KODE        = data.getString(GlobalConfig.GP_KODE);
                                    piutang.NAMA        = data.getString(GlobalConfig.GP_NAMA_TOKO);
                                    piutang.ALAMAT      = data.getString(GlobalConfig.GP_ALAMAT);
                                    piutang.PIUTANG     = Double.parseDouble(data.getString(GlobalConfig.GP_PIUTANG));
                                    piutang.JUMLAH_FAKTUR= data.getString(GlobalConfig.GP_JUMLAH_FAKTUR);
                                    piutang.TGL_JATUH_TEMPO= data.getString(GlobalConfig.GP_TGL_JATUH_TEMPO);

                                    list_datas.add(piutang);
                                    adapter.notifyDataSetChanged();
                                    swipeContainer.setRefreshing(false);
                                }
                            }else{
                                notifikasi("Data tidak ditemukan.");
                                swipeContainer.setRefreshing(false);
                            }
                        }else{
                            notifikasi(message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        swipeContainer.setRefreshing(false);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // do something
                    swipeContainer.setRefreshing(false);
                    NetworkResponse response = error.networkResponse;
                    if(response != null && response.data != null){
                        myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "code"+response.statusCode);
                        switch(response.statusCode){
                            case 404:
                                notifikasi("Terjadi masalah dengan server.");
                                break;
                            case 408:
                                notifikasi("Waktu terlalu lama untuk memproses, silakan ulangi lagi!");
                                break;
                            case 500:
                                notifikasi("Terjadi masalah dengan server.");
                                break;
                            default:
                                notifikasi("Mohon maaf terjadi kesalahan.");
                                break;
                        }
                    }
                }
            }){
                public Map<String, String> getHeaders() {
                    Map<String,String> headers = new Hashtable<String, String>();

                    //Adding parameters
                    headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }};

            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            requestQueue.add(request);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    private void notifikasi(String message){
        Snackbar snack = Snackbar.make(ll_main, message, Snackbar.LENGTH_LONG);
        snack.setActionTextColor(getResources().getColor(android.R.color.white )).show();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        //getMenuInflater().inflate(R.menu.menu_pelanggaran_santri, menu);
        return true;
    }
    private void cancelRequest(){
        if(request!=null) {
            requestQueue.cancelAll(request);
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelRequest();
    }
}
