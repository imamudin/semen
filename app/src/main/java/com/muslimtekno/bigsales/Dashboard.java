package com.muslimtekno.bigsales;

/**
 * Created by imamudin on 12/01/17.
 */

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.muslimtekno.bigsales.app.MyAppController;
import com.muslimtekno.bigsales.config.GlobalConfig;
import com.muslimtekno.bigsales.mysp.ObscuredSharedPreferences;
import com.muslimtekno.bigsales.util.MyLog;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Map;

public class Dashboard extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener,
        ResultCallback<LocationSettingsResult> {
    LinearLayout ll_main;
    TextView tv_nama_user, tv_target, tv_realisasi, tv_order, tv_total_transaksi, tv_bulan;
    TextView tv_prosesntasi_realisasi, tv_target_harian;
    LinearLayout ll_scan, ll_hOrder, ll_hKunjungan, ll_piutang, ll_pembayaran, ll_poin, ll_set_lokasi, ll_logout;
    boolean firt_time;
    ObscuredSharedPreferences pref;
    ProgressDialog loading;
    JsonObjectRequest request;
    MyLog myLog;

    //untuk filter
    LinearLayout ll_pilih_toko, ll_tgl_mulai, ll_tgl_selesai;
    Button btn_simpan, btn_kosongkan;
    TextView tv_nama_toko, tv_tgl_mulai, tv_tgl_selesai;
    Calendar c_awal, c_akhir;
    //tutup untuk filter

    //untuk mendapatakan lokasi
    Double longitude = 0.0, latitude = 0.0;
    protected static final int REQUEST_CHECK_SETTINGS           = 20;
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS    = 20000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest mLocationRequest;
    protected LocationSettingsRequest mLocationSettingsRequest;
    protected Location mCurrentLocation;

    TextView tv_koordinat;
    ProgressDialog pDialog=null;

    protected Boolean mRequestingLocationUpdates;
    protected String mLastUpdateTime;

    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 100;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 101;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard);

        init();
        init_filter();
        setTanggalHariIni();
        updateTarget();
    }
    private void init(){
        firt_time = true;
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getResources().getString(R.string.app_name));
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setHomeButtonEnabled(false);

        pref = new ObscuredSharedPreferences(Dashboard.this,
                this.getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );
        myLog = new MyLog(getLocalClassName());

        if(!pref.getBoolean(GlobalConfig.IS_LOGIN, false)){
            openLogin();
            myLog.print("login", "buka main");
        }
        if(pref.getBoolean(GlobalConfig.IS_TOKO, false)){
            openToko();
            myLog.print("toko", "buka toko");
        }


        ll_scan         = (LinearLayout)findViewById(R.id.ll_scan);
        ll_hKunjungan   = (LinearLayout)findViewById(R.id.ll_kunjungan);
        ll_piutang      = (LinearLayout)findViewById(R.id.ll_piutang);
        ll_hOrder       = (LinearLayout)findViewById(R.id.ll_order);
        ll_pembayaran   = (LinearLayout)findViewById(R.id.ll_pembayaran);
        ll_poin         = (LinearLayout)findViewById(R.id.ll_poin);
        ll_set_lokasi   = (LinearLayout)findViewById(R.id.ll_set_lokasi);
        ll_logout       = (LinearLayout)findViewById(R.id.ll_logout);


        tv_koordinat    = (TextView)findViewById(R.id.tv_koordinat);

        //untuk menampilkan target
        tv_nama_user    = (TextView)findViewById(R.id.tv_nama_user);
        tv_target       = (TextView)findViewById(R.id.tv_target);
        tv_realisasi    = (TextView)findViewById(R.id.tv_realisasi);
        tv_order        = (TextView)findViewById(R.id.tv_order);
        tv_total_transaksi= (TextView)findViewById(R.id.tv_total_transaksi);
        tv_bulan        = (TextView)findViewById(R.id.tv_bulan);

        tv_prosesntasi_realisasi= (TextView)findViewById(R.id.tv_prosentasi_realisasi);
        tv_target_harian        = (TextView)findViewById(R.id.tv_target_harian);

        ll_scan.setOnClickListener(btnClick);
        ll_hKunjungan.setOnClickListener(btnClick);
        ll_hOrder.setOnClickListener(btnClick);
        ll_piutang.setOnClickListener(btnClick);
        ll_pembayaran.setOnClickListener(btnClick);
        ll_poin.setOnClickListener(btnClick);
        ll_set_lokasi.setOnClickListener(btnClick);
        ll_logout.setOnClickListener(btnClick);

        ll_main     = (LinearLayout)findViewById(R.id.ll_main);

        getLokasiInit();
        mulaiGetLokasi();
    }

    private void init_filter(){
        ll_pilih_toko   = (LinearLayout)findViewById(R.id.ll_pilih_toko);
        ll_tgl_mulai    = (LinearLayout)findViewById(R.id.ll_tgl_mulai);
        ll_tgl_selesai  = (LinearLayout)findViewById(R.id.ll_tgl_selesai);

        btn_kosongkan   = (Button) findViewById(R.id.btn_kosongkan);
        btn_simpan      = (Button) findViewById(R.id.btn_simpan);

        tv_nama_toko    = (TextView) findViewById(R.id.tv_nama_toko);
        tv_tgl_mulai    = (TextView) findViewById(R.id.tv_tgl_mulai);
        tv_tgl_selesai  = (TextView) findViewById(R.id.tv_tgl_selesai);

        tv_tgl_mulai.setText(pref.getString(GlobalConfig.FILTER_TGL_MULAI_S,"-"));
        tv_tgl_selesai.setText(pref.getString(GlobalConfig.FILTER_TGL_AKHIR_S,"-"));

        ll_pilih_toko.setOnClickListener(btnClickfilter);
        ll_tgl_mulai.setOnClickListener(btnClickfilter);
        ll_tgl_selesai.setOnClickListener(btnClickfilter);
        btn_kosongkan.setOnClickListener(btnClickfilter);
        btn_simpan.setOnClickListener(btnClickfilter);

        //inisialia awal dilai calendar jika pref sudah ada
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try {
            myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "Set tanggal awal.");
            setTanggalHariIni();
        }catch (Exception e){
            myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "tidak dapat memproses tanggal.");
        }

        if(!pref.getString(GlobalConfig.FILTER_TOKOID, "").equals("")){
            tv_nama_toko.setText(pref.getString(GlobalConfig.FILTER_TOKO_NAMA,""));
        }
    }

    View.OnClickListener btnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent open = null;
            if(v==ll_scan){
                //check permission lokasi untuk marshmallow
                //mulaiGetLokasi();
                if(latitude == 0 || latitude == 0){
                    notifikasi("Silakan aktifkan GPS dan tunggu koordinat diperbarui.");
                }else {
                    startScan();
                }
                return;
            }else if(v==ll_hKunjungan){
                open = new Intent(Dashboard.this, HistoryKunjungan.class);
            }else if(v==ll_hOrder){
                showPilihHistory();
            }else if(v==ll_piutang){
                open = new Intent(Dashboard.this, DaftarPiutang.class);
            }else if(v==ll_pembayaran){
                open = new Intent(Dashboard.this, HistoryPembayaran.class);
            }else if(v==ll_poin){
                open = new Intent(Dashboard.this, HistoryPoin.class);
            }else if(v==ll_set_lokasi){
                if(latitude==0 && longitude==0) {
                    notifikasi("Koordinat lokasi tidak ditemukan.");
                }else {
                    Intent i_cari_toko = new Intent(Dashboard.this, PilihTokoSetLokasi.class);
                    //i_cari_toko.putExtra("search", s_nama_toko);
                    i_cari_toko.putExtra(GlobalConfig.GTOKO_LAT, latitude);
                    i_cari_toko.putExtra(GlobalConfig.GTOKO_LONG, longitude);
                    startActivity(i_cari_toko);
                }
                return;
            }else if(v==ll_logout){
                dialogLogout();
                return;
            }
            if(open != null){
                startActivity(open);
            }
        }
    };
    private void startScan(){
        if (ContextCompat.checkSelfPermission(Dashboard.this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            //request permission camera
            ActivityCompat.requestPermissions(Dashboard.this,
                    new String[]{Manifest.permission.CAMERA},
                    MY_PERMISSIONS_REQUEST_CAMERA);
        } else {
            scanQRCode();
        }
    }
    private void showPilihHistory(){
        LayoutInflater layoutInflater = (LayoutInflater) getLayoutInflater();
        final View promptView = layoutInflater.inflate(R.layout.pilih_jenis_order, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Dashboard.this);
        alertDialogBuilder.setView(promptView);

        final LinearLayout ll_order_semen     = (LinearLayout)promptView.findViewById(R.id.ll_order_semen);
        final LinearLayout ll_order_non_semen = (LinearLayout)promptView.findViewById(R.id.ll_order_non_semen);

        alertDialogBuilder.setNegativeButton("BATAL",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        final AlertDialog alert = alertDialogBuilder.create();
        alert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alert.setCancelable(true);
        alert.show();

        View.OnClickListener llOder = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v == ll_order_semen){
                    pref.edit().putBoolean(GlobalConfig.T_JENIS_ORDER_SEMEN, true).commit();
                }else if (v == ll_order_non_semen){
                    pref.edit().putBoolean(GlobalConfig.T_JENIS_ORDER_SEMEN, false).commit();
                }

                alert.dismiss();
                Intent open = new Intent(Dashboard.this, HistoryOrder.class);
                startActivity(open);
            }
        };

        ll_order_semen.setOnClickListener(llOder);
        ll_order_non_semen.setOnClickListener(llOder);
    }
    View.OnClickListener btnClickfilter = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v==btn_simpan){
                simpan();
                return;
            }else if(v==btn_kosongkan){
                Calendar c = Calendar.getInstance();
                tv_nama_toko.setText("");

                pref.edit().remove(GlobalConfig.FILTER_TOKOID).commit();
                pref.edit().remove(GlobalConfig.FILTER_TOKO_NAMA).commit();
                pref.edit().remove(GlobalConfig.FILTER_TOKO_ALAMAT).commit();

                setTanggalHariIni();

                Toast.makeText(Dashboard.this, "Filter telah dihapus.", Toast.LENGTH_SHORT).show();

//                setResult(RESULT_OK);
//                finish();
                return;
            }else if(v==ll_pilih_toko){
                Intent pilih_toko = new Intent(Dashboard.this, PilihToko.class);
                startActivityForResult(pilih_toko, GlobalConfig.KODE_INTENT_PILIH_TOKO);
                return;
            }else if(v==ll_tgl_mulai){
                dateTimePick(tv_tgl_mulai);
                return;
            }else if(v==ll_tgl_selesai){
                dateTimePick(tv_tgl_selesai);
                return;
            }
        }
    };
    private void  simpan(){
        if(c_awal == null || c_akhir==null || tv_tgl_mulai.getText().equals("-") || tv_tgl_selesai.getText().equals("-")){
            //notifikasi("Masukan tanggal mulai dan akhir.");
        }else {
            if (c_awal.getTimeInMillis() > c_akhir.getTimeInMillis()) {
                notifikasi("Tanggal mulai harus lebih rendah.");
            } else {View.OnClickListener btnClickfilter = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(v==btn_simpan){
                        simpan();
                        return;
                    }else if(v==btn_kosongkan){
                        Calendar c = Calendar.getInstance();
                        SimpleDateFormat format_s   = new SimpleDateFormat("dd MMM yyyy");
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

                        tv_tgl_mulai.setText(""+format_s.format(c.getTime()));
                        tv_tgl_selesai.setText(""+format_s.format(c.getTime()));
                        tv_nama_toko.setText("");

                        pref.edit().remove(GlobalConfig.FILTER_TOKOID).commit();
                        pref.edit().remove(GlobalConfig.FILTER_TOKO_NAMA).commit();
                        pref.edit().remove(GlobalConfig.FILTER_TOKO_ALAMAT).commit();

                        pref.edit().putString(GlobalConfig.FILTER_TGL_MULAI, ""+format.format(c_awal.getTime())).commit();
                        pref.edit().putString(GlobalConfig.FILTER_TGL_AKHIR, ""+format.format(c_akhir.getTime())).commit();
                        pref.edit().putString(GlobalConfig.FILTER_TGL_MULAI_S, ""+tv_tgl_mulai.getText()).commit();
                        pref.edit().putString(GlobalConfig.FILTER_TGL_AKHIR_S, ""+tv_tgl_selesai.getText()).commit();

                        setTanggalHariIni();

                        Toast.makeText(Dashboard.this, "Filter telah dihapus.", Toast.LENGTH_SHORT).show();

//                setResult(RESULT_OK);
//                finish();
                        return;
                    }else if(v==ll_pilih_toko){
                        Intent pilih_toko = new Intent(Dashboard.this, PilihToko.class);
                        startActivityForResult(pilih_toko, GlobalConfig.KODE_INTENT_PILIH_TOKO);
                        return;
                    }else if(v==ll_tgl_mulai){
                        dateTimePick(tv_tgl_mulai);
                        return;
                    }else if(v==ll_tgl_selesai){
                        dateTimePick(tv_tgl_selesai);
                        return;
                    }
                }
            };
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                //myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "tanggal : "+format.format(c_awal.getTime())+", akhir : "+format.format(c_akhir.getTime()));

                pref.edit().putString(GlobalConfig.FILTER_TGL_MULAI, ""+format.format(c_awal.getTime())).commit();
                pref.edit().putString(GlobalConfig.FILTER_TGL_AKHIR, ""+format.format(c_akhir.getTime())).commit();
                pref.edit().putString(GlobalConfig.FILTER_TGL_MULAI_S, ""+tv_tgl_mulai.getText()).commit();
                pref.edit().putString(GlobalConfig.FILTER_TGL_AKHIR_S, ""+tv_tgl_selesai.getText()).commit();
            }
        }
        Toast.makeText(Dashboard.this, "Filter telah disimpan.", Toast.LENGTH_SHORT).show();
        //setResult(RESULT_OK);
        //finish();
    }
    private void scanQRCode(){
        IntentIntegrator integrator = new IntentIntegrator(Dashboard.this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
        integrator.setPrompt("Silakan arahkan ke barcode toko.");
        integrator.setCameraId(0);  // Use a specific camera of the device
        integrator.setOrientationLocked(true);
        integrator.setBeepEnabled(true);
        integrator.setCaptureActivity(CaptureActivityPortrait.class);
        integrator.initiateScan();
    }
    @Override
    public void onRequestPermissionsResult
            (int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                myLog.print("Camera", "G : " + grantResults[0]);
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    myLog.print(getLocalClassName(), "akses kamera diizinkan");
                    scanQRCode();
                    return;
                } else {
                    myLog.print(getLocalClassName(), "akses kamera tidak diizinkan");

                    //It will return false if the user tapped “Never ask again”.
                    boolean is_Neveraskagain = ActivityCompat.shouldShowRequestPermissionRationale(this,
                            android.Manifest.permission.CAMERA);
                    if (is_Neveraskagain) {
                        //showAlert();
                        myLog.print(getLocalClassName(), "is_Neveraskagain false");
                    } else if (!is_Neveraskagain) {
                        myLog.print(getLocalClassName(), "is_Neveraskagain true");
                        if(!pref.getBoolean(GlobalConfig.PERMISIION_CAMERA_FIRST_TIME, false)){
                            //myLog.print(getLocalClassName(), "PERMISIION_CAMERA_FIRST_TIME : false");
                            pref.edit().putBoolean(GlobalConfig.PERMISIION_CAMERA_FIRST_TIME, true).commit();
                        }else{
                            //myLog.print(getLocalClassName(), "PERMISIION_CAMERA_FIRST_TIME : true");
                            startInstalledAppDetailsActivity(Dashboard.this);
                        }
                    }
                    return;
                }
            }
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                myLog.print("Camera", "G : " + grantResults[0]);
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    myLog.print(getLocalClassName(), "akses lokasi diizinkan");
                    checkLocationSettings();
                    return;
                } else {
                    myLog.print(getLocalClassName(), "akses lokasi tidak diizinkan");

                    //It will return false if the user tapped “Never ask again”.
                    boolean is_Neveraskagain = ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.CAMERA);
                    if (is_Neveraskagain) {
                        //showAlert();
                        myLog.print(getLocalClassName(), "is_Neveraskagain false");
                    } else if (!is_Neveraskagain) {
                        myLog.print(getLocalClassName(), "is_Neveraskagain true");
                        if(!pref.getBoolean(GlobalConfig.PERMISIION_LOCATION_FIRST_TIME, false)){
                            pref.edit().putBoolean(GlobalConfig.PERMISIION_LOCATION_FIRST_TIME, true).commit();
                        }else{
                            startInstalledAppDetailsActivity(Dashboard.this);
                        }
                    }
                    return;
                }
            }
        }
    }
    public static void startInstalledAppDetailsActivity(final Activity context) {
        if (context == null) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }
    private void openToko(String id_toko){
        if(latitude==0 && longitude==0){
            notifikasi("Koordinat lokasi tidak ditemukan.");
        }else{
            myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "scan toko");
            loading = ProgressDialog.show(Dashboard.this, "Memproses data", "Mohon tunggu...", true);

            String url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_SCANTOKO;

            myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(),""+url);
            JSONObject jsonBody;
            try {
                jsonBody = new JSONObject();
                jsonBody.put(GlobalConfig.USER_ID, pref.getString(GlobalConfig.USER_ID, ""));
                jsonBody.put(GlobalConfig.USER_TOKEN, pref.getString(GlobalConfig.USER_TOKEN, ""));

                jsonBody.put(GlobalConfig.ID_TOKO, id_toko);
                jsonBody.put(GlobalConfig.GTOKO_LAT, latitude);
                jsonBody.put(GlobalConfig.GTOKO_LONG, longitude);

                myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), ""+jsonBody.toString());
                request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        loading.dismiss();
                        myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), response.toString());
                        try {
                            int status      = response.getInt("status");
                            String message  = response.getString("message");
                            if(status==1){
                                JSONObject data = response.getJSONObject("data");

                                JSONObject toko = data.getJSONObject("poin");
                                pref.edit().putString(GlobalConfig.ID_TOKO, toko.getString(GlobalConfig.ID_TOKO)).commit();
                                pref.edit().putString(GlobalConfig.GTOKO_NAMA, toko.getString(GlobalConfig.GTOKO_NAMA)).commit();
                                pref.edit().putString(GlobalConfig.GTOKO_ALAMAT, toko.getString(GlobalConfig.GTOKO_ALAMAT)).commit();
                                pref.edit().putString(GlobalConfig.GTOKO_PEMILIK, toko.getString(GlobalConfig.GTOKO_PEMILIK)).commit();
                                pref.edit().putString(GlobalConfig.GTOKO_NOTELP, toko.getString(GlobalConfig.GTOKO_NOTELP)).commit();
                                pref.edit().putString(GlobalConfig.GREAL_PLAFON, toko.getString(GlobalConfig.GREAL_PLAFON)).commit();
                                pref.edit().putString(GlobalConfig.GKDSALES, toko.getString(GlobalConfig.GKDSALES)).commit();
                                pref.edit().putString(GlobalConfig.GJATUHTEMPOBAYAR, toko.getString(GlobalConfig.GJATUHTEMPOBAYAR)).commit();
                                pref.edit().putString(GlobalConfig.GTOKO_LAT, toko.getString(GlobalConfig.GTOKO_LAT)).commit();
                                pref.edit().putString(GlobalConfig.GTOKO_LONG, toko.getString(GlobalConfig.GTOKO_LONG)).commit();
                                pref.edit().putString(GlobalConfig.GTOKO_POIN, toko.getString(GlobalConfig.GTOKO_POIN)).commit();
                                pref.edit().putString(GlobalConfig.ID_TOKO, toko.getString(GlobalConfig.ID_TOKO)).commit();

                                JSONObject piutang = data.getJSONObject("piutang");
                                myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), piutang.getString(GlobalConfig.GTOKO_TOTAL_PIUTANG_KAP));
                                pref.edit().putString(GlobalConfig.GTOKO_TOTAL_PIUTANG_KAP, piutang.getString(GlobalConfig.GTOKO_TOTAL_PIUTANG_KAP)).commit();
                                pref.edit().putString(GlobalConfig.GTOKO_TGL_JATUH_TEMPO, piutang.getString(GlobalConfig.GTOKO_TGL_JATUH_TEMPO)).commit();
                                pref.edit().putString(GlobalConfig.GTOKO_REALPLAFON, piutang.getString(GlobalConfig.GTOKO_REALPLAFON)).commit();
                                pref.edit().putString(GlobalConfig.GTOKO_TAGIHAN_TERDEKAT, piutang.getString(GlobalConfig.GTOKO_TAGIHAN_TERDEKAT)).commit();

                                JSONObject kunjungan = data.getJSONObject("kunjungan");
                                pref.edit().putString(GlobalConfig.GKUNJUNGAN_TGL, kunjungan.getString(GlobalConfig.GKUNJUNGAN_TGL)).commit();
                                pref.edit().putString(GlobalConfig.GKUNJUNGAN_JAM_M, kunjungan.getString(GlobalConfig.GKUNJUNGAN_JAM_M)).commit();

                                //ditaruh akhir, karena apabila terjadi error pad salah satu data, maka session tidak tersimpan
                                pref.edit().putBoolean(GlobalConfig.IS_TOKO, true).commit();
                                Intent i_toko = new Intent(Dashboard.this, Toko.class);
                                startActivity(i_toko);
                            }else{
                                notifikasi(message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //myLog.print("respons",response.toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loading.dismiss();
                        NetworkResponse response = error.networkResponse;
                        if(response != null && response.data != null){
                            myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "code"+response.statusCode);
                            switch(response.statusCode){
                                case 404:
                                    displayMessage("Terjadi masalah dengan server.");
                                    break;
                                case 408:
                                    displayMessage("Waktu terlalu lama untuk memproses, silakan ulangi lagi!");
                                    break;
                                case 500:
                                    displayMessage("Terjadi masalah dengan server.");
                                    break;
                                default:
                                    displayMessage("Mohon maaf terjadi kesalahan.");
                                    break;
                            }
                        }
                    }
                }){
                    public Map<String, String> getHeaders() {
                        Map<String,String> headers = new Hashtable<String, String>();

                        //Adding parameters
                        headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        return headers;
                    }};

                request.setRetryPolicy(new DefaultRetryPolicy(
                        GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                MyAppController.getInstance().addToRequestQueue(request);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    public void updateTarget(){
        myLog.print(getLocalClassName(), "update target");

        tv_nama_user.setText(pref.getString(GlobalConfig.T_NAMA_PEGAWAI,"-"));
        tv_target.setText(pref.getString(GlobalConfig.T_TOTAL_TARGET,"-")+" sak");
        tv_realisasi.setText(pref.getString(GlobalConfig.T_TOTAL_REALISASI,"-")+" sak");
        double prosentasi_realisasi = (Double.parseDouble(pref.getString(GlobalConfig.T_TOTAL_REALISASI,"0"))
                / Double.parseDouble(pref.getString(GlobalConfig.T_TOTAL_TARGET,"1")) ) * 100;
        tv_prosesntasi_realisasi.setText(((int)Math.round(prosentasi_realisasi * 100)/(double)100)+" %");
        tv_target_harian.setText(
                ((int)Math.round((Double.parseDouble(pref.getString(GlobalConfig.T_TOTAL_TARGET,"0"))/24) * 100)/(double)100)+" sak");
        tv_order.setText(pref.getString(GlobalConfig.T_JUMLAH_ORDER,"-"));
        tv_total_transaksi.setText(decimalToRupiah(Double.parseDouble(pref.getString(GlobalConfig.T_TOTAL_TRANSAKSI,"0"))));

        tv_bulan.setText(pref.getString(GlobalConfig.T_BULAN,"-")+" "+pref.getString(GlobalConfig.T_TAHUN,"-"));
    }
    private String decimalToRupiah(double harga){
        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        kursIndonesia.setDecimalFormatSymbols(formatRp);

        String s_harga = kursIndonesia.format(harga);
        return s_harga.substring(0, (s_harga.length()-3));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(1, 1, 1, "REFRESH").setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        return super.onCreateOptionsMenu(menu);
    }
    public void displayMessage(String toastString){
        Toast.makeText(getApplicationContext(), toastString, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case 1:
                getTarget();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void getTarget(){
        loading = ProgressDialog.show(Dashboard.this, "", "Mohon tunggu...", true);

        String url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_TARGET;
        //myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(),""+url);
        JSONObject jsonBody;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(GlobalConfig.USER_ID, pref.getString(GlobalConfig.USER_ID, ""));
            jsonBody.put(GlobalConfig.USER_TOKEN, pref.getString(GlobalConfig.USER_TOKEN, ""));

            //myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), jsonBody.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loading.dismiss();
                    //myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "tutup loading");
                    try {
                        int status      = response.getInt("status");
                        String message  = response.getString("message");
                        if(status==1){
                            //hapus semua preferences
                            JSONObject data_target   = response.getJSONObject("target");
                            JSONObject target   = data_target.getJSONObject("data");
                            JSONObject waktu   = data_target.getJSONObject("waktu");

                            pref.edit().putString(GlobalConfig.T_NAMA_PEGAWAI, target.getString(GlobalConfig.T_NAMA_PEGAWAI)).commit();
                            pref.edit().putString(GlobalConfig.T_JUMLAH_ORDER, target.getString(GlobalConfig.T_JUMLAH_ORDER)).commit();
                            pref.edit().putString(GlobalConfig.T_TOTAL_TARGET, target.getString(GlobalConfig.T_TOTAL_TARGET)).commit();
                            pref.edit().putString(GlobalConfig.T_TOTAL_TRANSAKSI, target.getString(GlobalConfig.T_TOTAL_TRANSAKSI)).commit();
                            pref.edit().putString(GlobalConfig.T_TOTAL_REALISASI, target.getString(GlobalConfig.T_TOTAL_REALISASI)).commit();

                            pref.edit().putString(GlobalConfig.T_BULAN, waktu.getString(GlobalConfig.T_BULAN)).commit();
                            pref.edit().putString(GlobalConfig.T_TAHUN, waktu.getString(GlobalConfig.T_TAHUN)).commit();

                            Toast.makeText(getApplicationContext(),"Target Sales telah diperbarui.",Toast.LENGTH_LONG).show();
                            updateTarget();
                        }else{
                            notifikasi(message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //myLog.print("respons",response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // do something
                    loading.dismiss();
                    if(error!=null) {
                        //myLog.print("respons", error.getMessage().toString());
                    }
                }
            }){
                public Map<String, String> getHeaders() {
                    Map<String,String> headers = new Hashtable<String, String>();

                    //Adding parameters
                    headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }};

            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MyAppController.getInstance().addToRequestQueue(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void dateTimePick(final TextView editText){
        final View dialogView = View.inflate(this, R.layout.date_picker, null);
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        final DatePicker dp_awal = (DatePicker) dialogView.findViewById(R.id.date_picker);
        dp_awal.setMaxDate(System.currentTimeMillis());
        if(editText==tv_tgl_mulai){
            dp_awal.updateDate(c_awal.get(Calendar.YEAR),c_awal.get(Calendar.MONTH),c_awal.get(Calendar.DATE));
        }else {
            dp_awal.updateDate(c_akhir.get(Calendar.YEAR),c_akhir.get(Calendar.MONTH),c_akhir.get(Calendar.DATE));
            if (!tv_tgl_mulai.getText().toString().trim().equals("")) {
                String mytime = tv_tgl_mulai.getText().toString().trim();
                //myLog.print(getLocalClassName(), mytime);
                SimpleDateFormat dateFormat = new SimpleDateFormat(
                        "dd MMM yyyy");
                Date myDate = null;
                try {
                    myDate = dateFormat.parse(mytime);
                    dp_awal.setMinDate(myDate.getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                    myLog.print(getLocalClassName(), "error");
                }
            }else{
                dp_awal.updateDate(c_akhir.get(Calendar.YEAR),c_akhir.get(Calendar.MONTH),c_akhir.get(Calendar.DATE));
            }
        }

        dialogView.findViewById(R.id.date_time_set).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = new GregorianCalendar(dp_awal.getYear(),
                        dp_awal.getMonth(),
                        dp_awal.getDayOfMonth());

                if(editText==tv_tgl_mulai){
                    c_awal = cal;
                }else{
                    c_akhir = cal;
                }

                SimpleDateFormat format_show = new SimpleDateFormat("dd MMM yyyy");
                //myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "tanggal : "+format_show.format(cal.getTime()));
                editText.setText(format_show.format(cal.getTime()));

                //menyimpan perubahan tanggal dan nama toko
                simpan();

                alertDialog.dismiss();
            }});
        alertDialog.setView(dialogView);
        alertDialog.show();
    }
    private void notifikasi(String message){
        Snackbar snack = Snackbar.make(ll_main, message, Snackbar.LENGTH_LONG);
        snack.setActionTextColor(getResources().getColor(android.R.color.white )).show();
    }
    private void dialogLogout(){
        AlertDialog.Builder builder = new AlertDialog.Builder(Dashboard.this);
        // Set the dialog title
        builder.setTitle(getResources().getString(R.string.app_name))
                .setMessage("Apakah yakin anda akan keluar?")
                .setCancelable(false)
                .setNegativeButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        logout();
                    }
                })
                .setPositiveButton("Batal", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
        ;
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    private void logout(){
        myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "make progress dialog");
        loading = ProgressDialog.show(Dashboard.this, "Logout...", "Mohon tunggu...", true);

        String url = "";
        if(pref.getString(GlobalConfig.IP_KEY, null) != null){
            url = pref.getString(GlobalConfig.IP_KEY, "")+GlobalConfig.WEB_URL+GlobalConfig.URL_LOGOUT;
        }else{
            url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_LOGOUT;
        }
        myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(),""+url);
        JSONObject jsonBody;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(GlobalConfig.USER_ID, pref.getString(GlobalConfig.USER_ID, ""));
            jsonBody.put(GlobalConfig.USER_TOKEN, pref.getString(GlobalConfig.USER_TOKEN, ""));

            myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), jsonBody.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loading.dismiss();
                    myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "tutup loading");
                    try {
                        int status      = response.getInt("status");
                        String message  = response.getString("message");
                        if(status==1){
                            //hapus semua preferences
                            pref.edit().remove(GlobalConfig.USER_ID).commit();
                            pref.edit().remove(GlobalConfig.USER_TOKEN).commit();
                            pref.edit().remove(GlobalConfig.IS_LOGIN).commit();

                            pref.edit().remove(GlobalConfig.FILTER_TOKOID).commit();
                            pref.edit().remove(GlobalConfig.FILTER_TOKO_NAMA).commit();
                            pref.edit().remove(GlobalConfig.FILTER_TOKO_ALAMAT).commit();

                            pref.edit().remove(GlobalConfig.FILTER_TGL_MULAI).commit();
                            pref.edit().remove(GlobalConfig.FILTER_TGL_AKHIR).commit();
                            pref.edit().remove(GlobalConfig.FILTER_TGL_MULAI_S).commit();
                            pref.edit().remove(GlobalConfig.FILTER_TGL_AKHIR_S).commit();
                            openLogin();
                        }else{
                            notifikasi(message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //myLog.print("respons",response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // do something
                    loading.dismiss();
                    if(error!=null) {
                        //myLog.print("respons", error.getMessage().toString());
                    }
                }
            }){
                public Map<String, String> getHeaders() {
                    Map<String,String> headers = new Hashtable<String, String>();

                    //Adding parameters
                    headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }};

            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MyAppController.getInstance().addToRequestQueue(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void openLogin(){
        Intent login = new Intent(Dashboard.this, Login.class);
        startActivity(login);
        Dashboard.this.finish();
    }
    private void openToko(){
        Intent toko = new Intent(Dashboard.this, Toko.class);
        startActivity(toko);
        Dashboard.this.finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelRequest();
        if(pref.getBoolean(GlobalConfig.IS_LOGIN, false)) {
            stopLocationUpdates();
        }
    }
    private void cancelRequest(){
        if(request!=null) {
            MyAppController.getInstance().cancelPendingRequests(request);
        }
    }
    private void setTanggalHariIni(){
        myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "Set tanggal awal.");
        c_awal = Calendar.getInstance();
        c_awal.set(Calendar.DATE, 1);

        c_akhir = Calendar.getInstance();

        SimpleDateFormat format_s   = new SimpleDateFormat("dd MMM yyyy");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        tv_tgl_mulai.setText(""+format_s.format(c_awal.getTime()));
        tv_tgl_selesai.setText(""+format_s.format(c_akhir.getTime()));

        pref.edit().putString(GlobalConfig.FILTER_TGL_MULAI, ""+format.format(c_awal.getTime())).commit();
        pref.edit().putString(GlobalConfig.FILTER_TGL_AKHIR, ""+format.format(c_akhir.getTime())).commit();
        pref.edit().putString(GlobalConfig.FILTER_TGL_MULAI_S, ""+tv_tgl_mulai.getText()).commit();
        pref.edit().putString(GlobalConfig.FILTER_TGL_AKHIR_S, ""+tv_tgl_selesai.getText()).commit();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (scanResult != null) {
            if(scanResult.getContents()!=null) {
                //notifikasi(scanResult.getContents());
                String id_toko = scanResult.getContents();
                openToko(id_toko);
            }else{
            }
        } else {
        }
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        myLog.print(getLocalClassName(), "User agreed to make required location settings changes.");
                        startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        myLog.print(getLocalClassName(), "User chose not to make required location settings changes.");
                        //finish();
                        break;
                }
            case GlobalConfig.KODE_INTENT_PILIH_TOKO:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        tv_nama_toko.setText(pref.getString(GlobalConfig.FILTER_TOKO_NAMA,""));
                        //menyimpan perubahan tanggal dan nama toko
                        simpan();
                        break;
                }
                break;
        }
    }

    /*-----------------------------------untuk dapat lokasi---------------------------*/
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }
    private void getLokasiInit(){
        tv_koordinat        = (TextView)findViewById(R.id.tv_koordinat);
        mRequestingLocationUpdates = false;
        mLastUpdateTime = "";

        buildGoogleApiClient();
        createLocationRequest();
        buildLocationSettingsRequest();
    }
    private void mulaiGetLokasi(){
        if (ContextCompat.checkSelfPermission(Dashboard.this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            //request permission camera
            ActivityCompat.requestPermissions(Dashboard.this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_LOCATION);
        } else {
            checkLocationSettings();
        }
    }
    protected synchronized void buildGoogleApiClient() {
        myLog.print(getLocalClassName(), "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }
    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }
    protected void checkLocationSettings() {
        myLog.print(getLocalClassName(),"checkLocationSettings");
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );
        result.setResultCallback(this);
    }
    @Override
    public void onConnected(Bundle bundle) {
        myLog.print(getLocalClassName(), "Connected to GoogleApiClient");
        if (mCurrentLocation == null) {
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());

            //updateLocationUI();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        myLog.print(getLocalClassName(), "Location onConnectionSuspended");
    }
    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());

        updateLocationUI();
        myLog.print(getLocalClassName(), "Lokasi telah diperbarui");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        myLog.print(getLocalClassName(), "Connection failed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
    }

    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                myLog.print(getLocalClassName(), "All location settings are satisfied.");
                startLocationUpdates();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                myLog.print(getLocalClassName(), "Location settings are not satisfied. Show the user a dialog to" +
                        "upgrade location settings ");
                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(Dashboard.this, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    myLog.print(getLocalClassName(), "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                myLog.print(getLocalClassName(), "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created.");
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        stopLocationUpdates();
    }

    private void setpDialog(String log){
        myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), log);
        pDialog = new ProgressDialog(Dashboard.this);
        // Showing progress dialog before making http request
        pDialog.setMessage("Mohon tunggu…");
        pDialog.setCancelable(false);
        pDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Batal", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                stopLocationUpdates();
                pDialog.cancel();
            }
        });
        pDialog.show();
    }
    protected void startLocationUpdates() {
        setpDialog("startLocationUpdates");
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient,
                mLocationRequest,
                this
        ).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(Status status) {
                mRequestingLocationUpdates = true;
            }
        });
    }
    private GoogleMap.OnMyLocationChangeListener myLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {
        @Override
        public void onMyLocationChange(Location location) {
            LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
            latitude   = loc.latitude;
            longitude  = loc.longitude;
        }
    };
    private void updateLocationUI() {
        if (mCurrentLocation != null) {
            LatLng currentpos=new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
            latitude   = currentpos.latitude;
            longitude  = currentpos.longitude;

            //myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "lat : "+latitude+", long : "+longitude);
            tv_koordinat.setText(latitude+";"+longitude);
            //Toast.makeText(Dashboard.this, "Lokasi telah diperbarui.", Toast.LENGTH_SHORT).show();

            //mengilangkan progress dialog
            pDialog.dismiss();

            //stopLocationUpdates();
        }
    }

    protected void stopLocationUpdates() {
        if(mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient,
                    this
            ).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(Status status) {
                    mRequestingLocationUpdates = false;
                }
            });
        }
    }
    /*-----------------------------------untuk dapat lokasi---------------------------*/
}