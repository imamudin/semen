package com.muslimtekno.bigsales;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.muslimtekno.bigsales.config.GlobalConfig;
import com.muslimtekno.bigsales.mysp.ObscuredSharedPreferences;
import com.muslimtekno.bigsales.util.MyLog;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by imamudin on 12/03/17.
 */
public class Filter extends AppCompatActivity {
    LinearLayout ll_main, ll_pilih_toko, ll_tgl_mulai, ll_tgl_selesai;
    Button btn_simpan, btn_kosongkan;
    TextView tv_nama_toko, tv_tgl_mulai, tv_tgl_selesai;
    ObscuredSharedPreferences pref;
    Calendar c_awal, c_akhir;
    MyLog myLog;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filter);

        init();
    }
    private void init(){
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Filter");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(false);

        pref = new ObscuredSharedPreferences(Filter.this,
                this.getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );
        myLog = new MyLog(getLocalClassName());

        ll_main         = (LinearLayout)findViewById(R.id.ll_main);
        ll_pilih_toko   = (LinearLayout)findViewById(R.id.ll_pilih_toko);
        ll_tgl_mulai    = (LinearLayout)findViewById(R.id.ll_tgl_mulai);
        ll_tgl_selesai  = (LinearLayout)findViewById(R.id.ll_tgl_selesai);

        btn_kosongkan   = (Button) findViewById(R.id.btn_kosongkan);
        btn_simpan      = (Button) findViewById(R.id.btn_simpan);

        tv_nama_toko    = (TextView) findViewById(R.id.tv_nama_toko);
        tv_tgl_mulai    = (TextView) findViewById(R.id.tv_tgl_mulai);
        tv_tgl_selesai  = (TextView) findViewById(R.id.tv_tgl_selesai);

        tv_tgl_mulai.setText(pref.getString(GlobalConfig.FILTER_TGL_MULAI_S,"-"));
        tv_tgl_selesai.setText(pref.getString(GlobalConfig.FILTER_TGL_AKHIR_S,"-"));

        ll_pilih_toko.setOnClickListener(btnClick);
        ll_tgl_mulai.setOnClickListener(btnClick);
        ll_tgl_selesai.setOnClickListener(btnClick);
        btn_kosongkan.setOnClickListener(btnClick);
        btn_simpan.setOnClickListener(btnClick);


        //inisialia awal dilai calendar jika pref sudah ada
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try {
            String tgl_mulai    = pref.getString(GlobalConfig.FILTER_TGL_MULAI,"2017-01-01");
            String tgl_akhir    = pref.getString(GlobalConfig.FILTER_TGL_AKHIR,"2017-02-01");

            Date date_m = df.parse(tgl_mulai);
            c_awal = Calendar.getInstance();
            c_awal.setTime(date_m);

            Date date_a = df.parse(tgl_akhir);
            c_akhir = Calendar.getInstance();
            c_akhir.setTime(date_a);
        }catch (Exception e){
            myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "tidak dapat memproses tanggal.");
        }

        if(!pref.getString(GlobalConfig.FILTER_TOKOID, "").equals("")){
            tv_nama_toko.setText(pref.getString(GlobalConfig.FILTER_TOKO_NAMA,"Semua"));
        }
    }

    private void setTanggalHariIni(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat format     = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat format_s   = new SimpleDateFormat("dd MMM yyyy");

        pref.edit().putString(GlobalConfig.FILTER_TGL_MULAI, ""+format.format(c.getTime())).commit();
        pref.edit().putString(GlobalConfig.FILTER_TGL_AKHIR, ""+format.format(c.getTime())).commit();
        pref.edit().putString(GlobalConfig.FILTER_TGL_MULAI_S, ""+format_s.format(c.getTime())).commit();
        pref.edit().putString(GlobalConfig.FILTER_TGL_AKHIR_S, ""+format_s.format(c.getTime())).commit();
    }

    View.OnClickListener btnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v==btn_simpan){
                simpan();
                return;
            }else if(v==btn_kosongkan){
                Calendar c = Calendar.getInstance();
                SimpleDateFormat format_s   = new SimpleDateFormat("dd MMM yyyy");
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");


                tv_tgl_mulai.setText(""+format_s.format(c.getTime()));
                tv_tgl_selesai.setText(""+format_s.format(c.getTime()));
                tv_nama_toko.setText("Semua");

                pref.edit().remove(GlobalConfig.FILTER_TOKOID).commit();
                pref.edit().remove(GlobalConfig.FILTER_TOKO_NAMA).commit();
                pref.edit().remove(GlobalConfig.FILTER_TOKO_ALAMAT).commit();

                pref.edit().putString(GlobalConfig.FILTER_TGL_MULAI, ""+format.format(c_awal.getTime())).commit();
                pref.edit().putString(GlobalConfig.FILTER_TGL_AKHIR, ""+format.format(c_akhir.getTime())).commit();
                pref.edit().putString(GlobalConfig.FILTER_TGL_MULAI_S, ""+tv_tgl_mulai.getText()).commit();
                pref.edit().putString(GlobalConfig.FILTER_TGL_AKHIR_S, ""+tv_tgl_selesai.getText()).commit();

                setTanggalHariIni();

                setResult(RESULT_OK);
                finish();
                return;
            }else if(v==ll_pilih_toko){
                Intent pilih_toko = new Intent(Filter.this, PilihToko.class);
                startActivityForResult(pilih_toko, GlobalConfig.KODE_INTENT_PILIH_TOKO);
                return;
            }else if(v==ll_tgl_mulai){
                dateTimePick(tv_tgl_mulai);
                return;
            }else if(v==ll_tgl_selesai){
                dateTimePick(tv_tgl_selesai);
                return;
            }
        }
    };
    private void  simpan(){
        if(c_awal == null || c_akhir==null || tv_tgl_mulai.getText().equals("-") || tv_tgl_selesai.getText().equals("-")){
            //notifikasi("Masukan tanggal mulai dan akhir.");
        }else {
            if (c_awal.getTimeInMillis() > c_akhir.getTimeInMillis()) {
                notifikasi("Tanggal mulai harus lebih rendah.");
            } else {

                myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), tv_tgl_mulai.getText()+" === "+tv_tgl_selesai.getText());

                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "tanggal : "+format.format(c_awal.getTime())+", akhir : "+format.format(c_akhir.getTime()));

                pref.edit().putString(GlobalConfig.FILTER_TGL_MULAI, ""+format.format(c_awal.getTime())).commit();
                pref.edit().putString(GlobalConfig.FILTER_TGL_AKHIR, ""+format.format(c_akhir.getTime())).commit();
                pref.edit().putString(GlobalConfig.FILTER_TGL_MULAI_S, ""+tv_tgl_mulai.getText()).commit();
                pref.edit().putString(GlobalConfig.FILTER_TGL_AKHIR_S, ""+tv_tgl_selesai.getText()).commit();
            }
        }

        setResult(RESULT_OK);
        finish();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case GlobalConfig.KODE_INTENT_PILIH_TOKO:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        tv_nama_toko.setText(pref.getString(GlobalConfig.FILTER_TOKO_NAMA,""));
                        break;
                }
                break;
        }
    }
    private void dateTimePick(final TextView editText){
        final View dialogView = View.inflate(this, R.layout.date_picker, null);
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        final DatePicker dp_awal = (DatePicker) dialogView.findViewById(R.id.date_picker);
        dp_awal.setMaxDate(System.currentTimeMillis());
        if(editText==tv_tgl_mulai){
            dp_awal.setMinDate(0);
        }else {
            if (!tv_tgl_mulai.getText().toString().trim().equals("")) {
                String mytime = tv_tgl_mulai.getText().toString().trim();
                SimpleDateFormat dateFormat = new SimpleDateFormat(
                        "dd MMM yyyy");
                Date myDate = null;
                try {
                    myDate = dateFormat.parse(mytime);
                    dp_awal.setMinDate(myDate.getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }

        dialogView.findViewById(R.id.date_time_set).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = new GregorianCalendar(dp_awal.getYear(),
                        dp_awal.getMonth(),
                        dp_awal.getDayOfMonth());

                if(editText==tv_tgl_mulai){
                    c_awal = cal;
                }else{
                    c_akhir = cal;
                }

                SimpleDateFormat format_show = new SimpleDateFormat("dd MMM yyyy");
                myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "tanggal : "+format_show.format(cal.getTime()));
                editText.setText(format_show.format(cal.getTime()));
                alertDialog.dismiss();
            }});
        alertDialog.setView(dialogView);
        alertDialog.show();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //menu.add(1, 2, 1, "Hapus Filter").setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }
    public void displayMessage(String toastString){
        Toast.makeText(getApplicationContext(), toastString, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void notifikasi(String message){
        Snackbar snack = Snackbar.make(ll_main, message, Snackbar.LENGTH_LONG);
        snack.setActionTextColor(getResources().getColor(android.R.color.white )).show();
    }
    private void dateTimePick(){
        final View dialogView = View.inflate(Filter.this, R.layout.date_time_picker, null);
        final AlertDialog alertDialog = new AlertDialog.Builder(Filter.this).create();

        dialogView.findViewById(R.id.date_time_set).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DatePicker datePicker = (DatePicker) dialogView.findViewById(R.id.date_picker);
                long time= System.currentTimeMillis();
                datePicker.setMinDate(time);

                Calendar c = new GregorianCalendar(datePicker.getYear(),
                        datePicker.getMonth(),
                        datePicker.getDayOfMonth());

                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), format.format(c.getTime()));
                //et_tanggal_kirim.setText(format.format(c.getTime()));
                alertDialog.dismiss();
            }});
        alertDialog.setView(dialogView);
        alertDialog.show();
    }
    private void notifikasi_dialog(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(Filter.this);
        builder.setTitle(title)
                .setCancelable(false)
                .setMessage(message)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
