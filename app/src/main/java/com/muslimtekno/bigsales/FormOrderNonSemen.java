package com.muslimtekno.bigsales;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.muslimtekno.bigsales.app.MyAppController;
import com.muslimtekno.bigsales.config.GlobalConfig;
import com.muslimtekno.bigsales.model.ProdukNonSemen;
import com.muslimtekno.bigsales.mysp.ObscuredSharedPreferences;
import com.muslimtekno.bigsales.util.MyLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/**
 * Created by imamudin on 15/12/16.
 */
public class FormOrderNonSemen extends AppCompatActivity {
    LinearLayout ll_main;
    TextView tv_jatuh_tempo, tv_plafon;
    EditText et_tanggal_kirim, et_total, et_tanggal;
    TextView tv_nama_toko, tv_poin, tv_alamat, tv_piutang;
    Button btn_simpan;
    ObscuredSharedPreferences pref;
    Double total_piutang;
    ProgressDialog loading;
    Spinner sp_gudang;
    HashMap<String,String> gudangMap;
    JsonObjectRequest request;
    String default_gudang;
    MyLog myLog;
    LinearLayout ll_tambah_edit_produk, ll_produks;
    boolean ada_faktur = true;
    TextView tv_total_order;

    List<ProdukNonSemen> list_produk_non_semen = new ArrayList<ProdukNonSemen>();
    ArrayAdapter<ProdukNonSemen> produk_non_semen_adapter;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_order_non_semen);

        init();
    }
    private void init(){
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("ORDER NON SEMEN");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(false);

        pref = new ObscuredSharedPreferences(FormOrderNonSemen.this,
                this.getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );
        myLog = new MyLog(getLocalClassName());

        ll_main         = (LinearLayout)findViewById(R.id.ll_main);
        ll_tambah_edit_produk = (LinearLayout)findViewById(R.id.ll_tambah_edit_produk);
        ll_produks      = (LinearLayout)findViewById(R.id.ll_produks);

        tv_alamat       = (TextView)findViewById(R.id.tv_alamat);
        tv_nama_toko    = (TextView)findViewById(R.id.tv_nama_toko);
        tv_piutang      = (TextView)findViewById(R.id.tv_piutang);
        tv_jatuh_tempo  = (TextView)findViewById(R.id.tv_jatuh_tempo);
        tv_plafon       = (TextView)findViewById(R.id.tv_plafon);
        tv_poin         = (TextView)findViewById(R.id.tv_poin);
        tv_total_order  = (TextView)findViewById(R.id.tv_total_order);

        tv_nama_toko.setText(pref.getString(GlobalConfig.GTOKO_NAMA,""));
        tv_alamat.setText(pref.getString(GlobalConfig.GTOKO_ALAMAT,""));
        tv_poin.setText(pref.getString(GlobalConfig.GTOKO_POIN,""));
        tv_piutang.setText(decimalToRupiah(Double.parseDouble(pref.getString(GlobalConfig.GTOKO_TOTAL_PIUTANG_KAP,"0"))));
        tv_jatuh_tempo.setText(pref.getString(GlobalConfig.GTOKO_TGL_JATUH_TEMPO,""));
        tv_plafon.setText(decimalToRupiah(Double.parseDouble(pref.getString(GlobalConfig.GREAL_PLAFON,"0"))));

        et_tanggal_kirim= (EditText) findViewById(R.id.et_tanggal_kirim);
        et_tanggal      = (EditText) findViewById(R.id.et_tanggal);
        et_total        = (EditText) findViewById(R.id.et_total);
        sp_gudang       = (Spinner)findViewById(R.id.sp_gudang);

        Intent old  = getIntent();
        ada_faktur      = old.getBooleanExtra(GlobalConfig.GADA_FAKTUR,true);
        total_piutang   = old.getDoubleExtra(GlobalConfig.GTOKO_TOTAL_PIUTANG, 0);
        default_gudang  = old.getStringExtra(GlobalConfig.GDEFAULT_GUDANG);
        try {
            JSONArray produks = new JSONArray(old.getStringExtra(GlobalConfig.GPRODUK_NON_SEMEN));
            for(int i=0; i<produks.length(); i++){
                String produk = produks.getString(i);
                ProdukNonSemen i_data = new ProdukNonSemen(produk);
                list_produk_non_semen.add(i_data);
            }

        }catch (Exception e){
            Log.d(GlobalConfig.TAG+"/"+getLocalClassName(), "Cannot process json array produk");
        }
        //untuk produk non semen
        produk_non_semen_adapter = new ArrayAdapter<ProdukNonSemen>(this, android.R.layout.simple_spinner_item, list_produk_non_semen);
        produk_non_semen_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        gudangMap = new HashMap<String, String>();

        //Log.d(GlobalConfig.TAG+"/"+getLocalClassName(), old.getStringExtra(GlobalConfig.GGUDANG));
        try {
            JSONArray gudangs = new JSONArray(old.getStringExtra(GlobalConfig.GGUDANG));
            List<String> gudanglist = new ArrayList<String>();
            String nama_defaul_gudang = "";
            if(gudangs.length()>0){
                for (int i = 0; i < gudangs.length(); i++) {
                    JSONObject gudang = gudangs.getJSONObject(i);
                    gudanglist.add(gudang.getString(GlobalConfig.GNAMA_GUDANG));
                    gudangMap.put(gudang.getString(GlobalConfig.GNAMA_GUDANG), gudang.getString(GlobalConfig.GID_GUDANG));

                    //untuk mencari defaul gudang
                    if(gudang.getString(GlobalConfig.GID_GUDANG).equals(default_gudang)){
                        nama_defaul_gudang = gudang.getString(GlobalConfig.GNAMA_GUDANG);
                    }
                }
                //santri
                ArrayAdapter<String> santriAdapater = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, gudanglist);
                santriAdapater.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                sp_gudang.setAdapter(santriAdapater);
                if(!nama_defaul_gudang.equals("")){
                    int position_defaul = santriAdapater.getPosition(nama_defaul_gudang);
                    sp_gudang.setSelection(position_defaul);
                }
            }
        }catch (Exception e){
            Log.d(GlobalConfig.TAG+"/"+getLocalClassName(), "Cannot process json array");
        }

        btn_simpan      = (Button)findViewById(R.id.btn_simpan);

        et_tanggal.setOnClickListener(btnClick);
        btn_simpan.setOnClickListener(btnClick);
        ll_tambah_edit_produk.setOnClickListener(btnClick);
    }
    View.OnClickListener btnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v==btn_simpan){
                if(hitungTotalOrder()>0 && et_tanggal.getText().length()>0){
                    ///kirimOrder();
                    cekSelisih();
                }else{
                    notifikasi("Silakan lengkapi semua form.");
                }
            }else if(v==et_tanggal){
                dateTimePick();
                return;
            }else if(v == ll_tambah_edit_produk){
                showTambahProduk();
            }
        }
    };
    private void showTambahProduk(){
        LayoutInflater layoutInflater = (LayoutInflater) getLayoutInflater();
        final View promptView = layoutInflater.inflate(R.layout.tambah_produk_non_semen, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(FormOrderNonSemen.this);
        alertDialogBuilder.setView(promptView);

        final Spinner sp_produk   = (Spinner)promptView.findViewById(R.id.sp_produk);
        final EditText et_harga   = (EditText)promptView.findViewById(R.id.et_harga);
        final EditText et_jumlah  = (EditText)promptView.findViewById(R.id.et_jumlah);
        final TextView tv_total   = (TextView)promptView.findViewById(R.id.tv_total);
        final Button btn_cancel   = (Button)promptView.findViewById(R.id.btn_batal);
        final Button btn_tambah   = (Button)promptView.findViewById(R.id.btn_tambah);


        final AlertDialog alert = alertDialogBuilder.create();
        alert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alert.setCancelable(false);
        alert.show();

        sp_produk.setAdapter(produk_non_semen_adapter);

        sp_produk.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if(position<=0) {
                    position = 1;
                }
                ProdukNonSemen produk = produk_non_semen_adapter.getItem(position);
                if(produk.HARGA != 0)
                    et_harga.setText(""+produk.HARGA);
                else
                    et_harga.setText("");
                et_jumlah.setText("");
                tv_total.setText(decimalToRupiah(hitungTotalItem(et_harga, et_jumlah)));
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        et_harga.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tv_total.setText(decimalToRupiah(hitungTotalItem(et_harga, et_jumlah)));
            }
        });
        et_jumlah.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tv_total.setText(decimalToRupiah(hitungTotalItem(et_harga, et_jumlah)));
            }
        });

        View.OnClickListener btnTambah = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v == btn_tambah){
                    double total = hitungTotalItem(et_harga, et_jumlah);
                    if(total > 0){
                        ProdukNonSemen produk = produk_non_semen_adapter.getItem(sp_produk.getSelectedItemPosition());
                        String s_harga  = et_harga.getText().toString();
                        String s_jumlah = et_jumlah.getText().toString();

                        addPordukNonSemen(produk, Integer.parseInt(s_jumlah), Integer.parseInt(s_harga), total);
                        alert.dismiss();
                    }else {
                        notifikasi("Silakan isi semua form!");
                    }
                }else {
                    alert.dismiss();
                }
            }
        };

        btn_cancel.setOnClickListener(btnTambah);
        btn_tambah.setOnClickListener(btnTambah);
    }
    private void addPordukNonSemen(ProdukNonSemen produk, int produk_jumlah, int produk_harga, double produk_total){
        LayoutInflater inflater = (LayoutInflater)this.getLayoutInflater();
        final View stok_view          = inflater.inflate(R.layout.item_produk_non_semen, null);

        final TextView tv_produk_id         =(TextView)stok_view.findViewById(R.id.tv_produk_id);
        final TextView tv_produk_nama       =(TextView)stok_view.findViewById(R.id.tv_produk_nama_non_semen);
        final TextView tv_produk_harga      =(TextView)stok_view.findViewById(R.id.tv_produk_harga);
        final TextView tv_produk_harga_show =(TextView)stok_view.findViewById(R.id.tv_produk_harga_non_semen);
        final TextView tv_produk_jumlah     =(TextView)stok_view.findViewById(R.id.tv_produk_jumlah);
        final TextView tv_produk_jumlah_show=(TextView)stok_view.findViewById(R.id.tv_produk_jumlah_non_semen);
        final TextView tv_produk_total      =(TextView)stok_view.findViewById(R.id.tv_produk_total);
        final TextView tv_produk_total_show =(TextView)stok_view.findViewById(R.id.tv_produk_total_non_semen);

        final LinearLayout ll_clear         = (LinearLayout) stok_view.findViewById(R.id.ll_clear);

        tv_produk_id.setText(""+produk.ID_PRODUK);
        tv_produk_nama.setText(produk.NAMA_PRODUK);
        tv_produk_harga.setText(""+produk_harga);
        tv_produk_harga_show.setText(decimalToRupiah(produk_harga));
        tv_produk_jumlah.setText(""+produk_jumlah);
        tv_produk_jumlah_show.setText(""+produk_jumlah);
        tv_produk_total.setText(""+produk_total);
        tv_produk_total_show.setText(""+decimalToRupiah(produk_total));

        //melakukan pengecekan apakah produk pernah ditambah, jika iya, hapus yang sebelumnya
        checkPenambahanProduk(produk.ID_PRODUK);

        ll_produks.addView(stok_view);
        tv_total_order.setText(decimalToRupiah(hitungTotalOrder()));

        ll_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_produks.removeView(stok_view);

                tv_total_order.setText(decimalToRupiah(hitungTotalOrder()));
            }
        });
    }
    private void checkPenambahanProduk(int produk_id){
        for(int i=0; i<ll_produks.getChildCount(); i++){
            LinearLayout item = (LinearLayout) ll_produks.getChildAt(i);
            TextView tv_produk_id = (TextView) item.getChildAt(0);

            if(produk_id == Integer.parseInt(tv_produk_id.getText().toString())){
                ll_produks.removeViewAt(i);
                break;
            }
        }
    }
    private double hitungTotalItem(EditText et_harga, EditText et_jumlah){
        String s_harga  = et_harga.getText().toString();
        String s_jumlah = et_jumlah.getText().toString();
        if(s_harga.length() <= 0){
            s_harga = "0";
        }
        if(s_jumlah.length() <= 0){
            s_jumlah = "0";
        }
        return Double.parseDouble(s_harga)
                * Double.parseDouble(s_jumlah);

    }
    private double hitungTotalOrder(){
        double total_order = 0;
        for(int i=0; i<ll_produks.getChildCount(); i++){
            LinearLayout item = (LinearLayout) ll_produks.getChildAt(i);
            TextView tv_total = (TextView) item.getChildAt(3);

            total_order += Double.parseDouble(tv_total.getText().toString());
        }
        return total_order;
    }
    private String decimalToRupiah(double harga){
        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        kursIndonesia.setDecimalFormatSymbols(formatRp);

        String hasil = kursIndonesia.format(harga);
        return hasil.substring(0, (hasil.length()-3));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //menu.add(1, 2, 1, "Hapus Filter").setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }
    private void cekSelisih(){
        double total_harga = hitungTotalOrder();
        double selisih = Double.parseDouble(pref.getString(GlobalConfig.GREAL_PLAFON,"0"))-(total_piutang + total_harga);

        if(selisih >= 0){
            kirimOrder(1);
        }else{
            //tidak bisa order
            String message  = "Total tagihan   = "+decimalToRupiah((total_piutang + total_harga))+""+System.getProperty("line.separator");
            message        += "Maksimum Plafon = "+decimalToRupiah(Double.parseDouble(pref.getString(GlobalConfig.GREAL_PLAFON,"0")))+System.getProperty("line.separator")+System.getProperty("line.separator");
            message        += "Order pelanggan selanjutnya menunggu approval dari perusahaan, Apakah anda melanjutkan proses order?";

            AlertDialog.Builder builder = new AlertDialog.Builder(FormOrderNonSemen.this);
            builder.setTitle("")
                    .setCancelable(false)
                    .setMessage(message)
                    .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            notifikasi_dialog("","Order dibatalkan.");
                        }
                    }).setPositiveButton("Lanjutkan", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    kirimOrder(0);
                }
            });;
            AlertDialog dialog = builder.create();
            dialog.show();

        }
    }
    private void kirimOrder(int is_approval){
        if(!ada_faktur){
            is_approval =  0;
        }

        String url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_SIMPANORDER_NON_SEMEN;
        Log.d(GlobalConfig.TAG+"/"+getLocalClassName(),""+url);

        double total_harga = hitungTotalOrder();

        String a_nama = sp_gudang.getSelectedItem().toString();
        String gudang_id = gudangMap.get(a_nama);

        //double selisih = Double.parseDouble(pref.getString(GlobalConfig.GREAL_PLAFON,"0"))-(total_piutang + total_harga);

        //Log.d(GlobalConfig.TAG+"/"+getLocalClassName(), ""+selisih);
//        Log.d(GlobalConfig.TAG+"/"+getLocalClassName(), "plafon : "+Double.parseDouble(pref.getString(GlobalConfig.GREAL_PLAFON,"0"))+
//        ", piutang : "+total_piutang+",harga : "+total_harga);

        if(loading == null || !loading.isShowing()) {
            Log.d(getLocalClassName(), "make progress dialog2");
            loading = ProgressDialog.show(FormOrderNonSemen.this, "Mengirim order", "Mohon tunggu...", true);
        }
        //bisa order
        JSONObject jsonBody;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(GlobalConfig.USER_ID, pref.getString(GlobalConfig.USER_ID, ""));
            jsonBody.put(GlobalConfig.USER_TOKEN, pref.getString(GlobalConfig.USER_TOKEN, ""));

            final JSONArray json_id_produk  = new JSONArray();
            final JSONArray json_jumlah     = new JSONArray();
            final JSONArray json_harga      = new JSONArray();

            boolean status_ada_harga_nol = false;
            for(int i=0;i<ll_produks.getChildCount();i++){
                LinearLayout layer0     = (LinearLayout) ll_produks.getChildAt(i);
                //semua ini melihat struktur pada item_produk_non_semen.xml
                //untuk mencari jumlah dan id produk
                TextView tv_produk_id   = (TextView) layer0.getChildAt(0);
                //untuk mencari harga
                TextView tv_harga_dec   = (TextView) layer0.getChildAt(2);

                String s_harga = tv_harga_dec.getText().toString().trim();
                if(s_harga.equals("")) s_harga ="0";

                double harga = Double.parseDouble(s_harga);

                json_id_produk.put(i,tv_produk_id.getText().toString());
                json_harga.put(i,harga);

                TextView tv_jumlah   = (TextView) layer0.getChildAt(1);
                String jumlah_pesan = tv_jumlah.getText().toString();
                if(jumlah_pesan.equals("")){
                    jumlah_pesan="0";
                }
                json_jumlah.put(i,Integer.parseInt(jumlah_pesan));

                //untuk mengecek apabila ada harga yang diisi nol dan jumlahnya lebih dari nol
                if(harga <= 0 && Integer.parseInt(jumlah_pesan) > 0){
                    status_ada_harga_nol = true;
                }
            }

            if(status_ada_harga_nol){
                notifikasi("Silakan isi harga semen!");
                loading.dismiss();
                loading = null;
            }else {
                jsonBody.put(GlobalConfig.GPRODUK_ID, json_id_produk);
                jsonBody.put(GlobalConfig.JUMLAH_PESANAN, json_jumlah);
                jsonBody.put(GlobalConfig.GHARGA, json_harga);

                jsonBody.put(GlobalConfig.TANGGAL_KIRIM, et_tanggal_kirim.getText().toString().trim());
                jsonBody.put(GlobalConfig.TOTAL_HARGA, total_harga);
                jsonBody.put(GlobalConfig.GKODE_GUDANG, gudang_id);
                jsonBody.put(GlobalConfig.ID_TOKO, pref.getString(GlobalConfig.ID_TOKO, ""));
                jsonBody.put(GlobalConfig.IS_APPROVAL, is_approval);

                Log.d(GlobalConfig.TAG+"/"+getLocalClassName(), jsonBody.toString());
                request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        loading.dismiss();
                        Log.d(GlobalConfig.TAG+"/"+getLocalClassName(), "tutup loading");
                        try {
                            int status = response.getInt("status");
                            String message = response.getString("message");
                            if (status == 1) {
                                dialogNotifikasi("Data Order berhasil disimpan.\nTerima kasih.");
                            } else {
                                notifikasi(message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //Log.d("respons",response.toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loading.dismiss();
                        NetworkResponse response = error.networkResponse;
                        if (response != null && response.data != null) {
                            Log.d(GlobalConfig.TAG+"/"+getLocalClassName(), "code" + response.statusCode);
                            switch (response.statusCode) {
                                case 404:
                                    displayMessage("Terjadi masalah dengan server.");
                                    break;
                                case 408:
                                    displayMessage("Waktu terlalu lama untuk memproses, silakan ulangi lagi!");
                                    break;
                                case 500:
                                    displayMessage("Terjadi masalah dengan server.");
                                    break;
                                default:
                                    displayMessage("Mohon maaf terjadi kesalahan.");
                                    break;
                            }
                        }
                    }
                }) {
                    public Map<String, String> getHeaders() {
                        Map<String, String> headers = new Hashtable<String, String>();

                        //Adding parameters
                        headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        return headers;
                    }
                };

                request.setRetryPolicy(new DefaultRetryPolicy(
                        GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                MyAppController.getInstance().addToRequestQueue(request);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void displayMessage(String toastString){
        Toast.makeText(getApplicationContext(), toastString, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void notifikasi(String message){
        Snackbar snack = Snackbar.make(ll_main, message, Snackbar.LENGTH_LONG);
        snack.setActionTextColor(getResources().getColor(android.R.color.white )).show();
        Toast.makeText(FormOrderNonSemen.this, message, Toast.LENGTH_SHORT).show();
    }
    private void dateTimePick(){
        final View dialogView = View.inflate(FormOrderNonSemen.this, R.layout.date_time_picker, null);
        final AlertDialog alertDialog = new AlertDialog.Builder(FormOrderNonSemen.this).create();

        dialogView.findViewById(R.id.date_time_set).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DatePicker datePicker = (DatePicker) dialogView.findViewById(R.id.date_picker);
                GregorianCalendar gc = new GregorianCalendar();
                gc.add(Calendar.DATE, 1);
                long time = gc.getTimeInMillis();
                datePicker.setMinDate(time);

                Calendar c = new GregorianCalendar(datePicker.getYear(),
                        datePicker.getMonth(),
                        datePicker.getDayOfMonth());

                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat format_tampil = new SimpleDateFormat("dd MMM yyyy");
                et_tanggal.setText(format_tampil.format(c.getTime()));
                et_tanggal_kirim.setText(format.format(c.getTime()));
                alertDialog.dismiss();
            }});
        alertDialog.setView(dialogView);
        alertDialog.show();
    }
    public void dialogNotifikasi(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(FormOrderNonSemen.this);
        // Set the dialog title
        builder.setTitle(getResources().getString(R.string.app_name))
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Intent toko = new Intent(FormOrderNonSemen.this, Toko.class);
                        startActivity(toko);
                        myLog.print("start toko");
                        FormOrderNonSemen.this.finish();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    private void notifikasi_dialog(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(FormOrderNonSemen.this);
        builder.setTitle(title)
                .setCancelable(false)
                .setMessage(message)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelRequest();
    }
    private void cancelRequest(){
        if(request!=null) {
            MyAppController.getInstance().cancelPendingRequests(request);
        }
    }
}