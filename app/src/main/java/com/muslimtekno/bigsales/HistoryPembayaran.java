package com.muslimtekno.bigsales;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.muslimtekno.bigsales.config.GlobalConfig;
import com.muslimtekno.bigsales.listAdapter.ListAdapterPembayaran;
import com.muslimtekno.bigsales.model.Pembayaran;
import com.muslimtekno.bigsales.mysp.ObscuredSharedPreferences;
import com.muslimtekno.bigsales.util.MyLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/**
 * Created by imamudin on 02/04/17.
 */
public class HistoryPembayaran extends AppCompatActivity {

    List<Pembayaran> list_datas = new ArrayList<Pembayaran>();
    ListView lv_kota;
    ListAdapterPembayaran adapter;
    RequestQueue requestQueue = null;

    ObscuredSharedPreferences pref;
    JsonObjectRequest request =null;
    private SwipeRefreshLayout swipeContainer;
    LinearLayout ll_main, ll_filter;

    int offSet=0;
    Handler handler;
    Runnable runnable;
    Boolean disableSwipeDown = false;       //untuk mendisable swipe down list view
    ProgressDialog loading;
    MyLog myLog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history_pembayaran);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("History Pembayaran");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(false);

        pref = new ObscuredSharedPreferences(this,
                this.getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );
        myLog = new MyLog(getLocalClassName());

        ll_main     =   (LinearLayout)findViewById(R.id.ll_main);
        ll_filter   =   (LinearLayout)findViewById(R.id.ll_filter);


        lv_kota = (ListView)findViewById(R.id.custom_list);
        list_datas.clear();
        adapter = new ListAdapterPembayaran(HistoryPembayaran.this, list_datas);
        lv_kota.setAdapter(adapter);

        ll_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent filter = new Intent(HistoryPembayaran.this, Filter.class);
                startActivityForResult(filter, GlobalConfig.KODE_INTENT_FILTER_ORDER);
            }
        });

        swipeContainer = (SwipeRefreshLayout)findViewById(R.id.swipeContainer);

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                calllist_datas(0);
                Toast.makeText(HistoryPembayaran.this,"refresh",Toast.LENGTH_LONG).show();
            }
        });
        swipeContainer.post(new Runnable() {
            @Override
            public void run() {
                swipeContainer.setRefreshing(true);
                calllist_datas(0);
            }
        });
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        lv_kota.setOnScrollListener(new AbsListView.OnScrollListener() {

            private int currentVisibleItemCount;
            private int currentScrollState;
            private int currentFirstVisibleItem;
            private int totalItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                this.currentScrollState = scrollState;
                this.isScrollCompleted();
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                this.currentFirstVisibleItem = firstVisibleItem;
                this.currentVisibleItemCount = visibleItemCount;
                this.totalItem = totalItemCount;
            }
            private void isScrollCompleted() {
                if (totalItem - currentFirstVisibleItem == currentVisibleItemCount
                        && this.currentScrollState == SCROLL_STATE_IDLE) {
                    if(!disableSwipeDown) {
                        swipeContainer.setRefreshing(true);
                        handler = new Handler();

                        runnable = new Runnable() {
                            public void run() {
                                calllist_datas(offSet);
                            }
                        };
                        //untuk menerlambatkan 0 detik
                        handler.postDelayed(runnable, 000);
                    }else{
                        //Toast.makeText(HistoryPembayaran.this,"Data telah ditampilkan semua.",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case GlobalConfig.KODE_INTENT_FILTER_ORDER:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        calllist_datas(0);
                        break;
                }
                break;
        }
    }
    private String decimalToRupiah(double harga){
        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        kursIndonesia.setDecimalFormatSymbols(formatRp);

        return kursIndonesia.format(harga);
    }
    private void calllist_datas(int page){
        if(page==0){
            list_datas.clear();
            adapter.notifyDataSetChanged();
            offSet=0;
            disableSwipeDown = false;
        }
        swipeContainer.setRefreshing(true);
        requestQueue = Volley.newRequestQueue(this);

        // Creating volley request obj
        String url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_PEMBAYARANS;
        myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(),""+url);
        JSONObject jsonBody;
        jsonBody = new JSONObject();
        try {
            jsonBody.put(GlobalConfig.USER_ID, pref.getString(GlobalConfig.USER_ID, ""));
            jsonBody.put(GlobalConfig.USER_TOKEN, pref.getString(GlobalConfig.USER_TOKEN, ""));

            jsonBody.put(GlobalConfig.UP_TGL_AWAL, pref.getString(GlobalConfig.FILTER_TGL_MULAI, ""));
            jsonBody.put(GlobalConfig.UP_TGL_AKHIR, pref.getString(GlobalConfig.FILTER_TGL_AKHIR, ""));

            jsonBody.put(GlobalConfig.UP_START, page);
            jsonBody.put(GlobalConfig.UP_LIMIT, GlobalConfig.MAX_ROW_PER_REQUEST);

            myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), jsonBody.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    swipeContainer.setRefreshing(false);
                    myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), response.toString());
                    try {
                        int status      = response.getInt("status");
                        String message  = response.getString("message");
                        if(status==1){
                            JSONObject respon_data  = response.getJSONObject("data");
                            int total               = respon_data.getInt("total");

                            if(total<GlobalConfig.MAX_ROW_PER_REQUEST || total==0) {
                                disableSwipeDown = true;
                            }
                            offSet += total;
                            JSONArray datas = respon_data.getJSONArray("pembayaran");
                            if(datas.length()>0) {
                                for (int i = 0; i < datas.length(); i++) {
                                    JSONObject data = datas.getJSONObject(i);
                                    myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), data.toString());

                                    Pembayaran i_data = new Pembayaran();

                                    i_data.pembayaranJson   =  data.toString();
                                    i_data.NAMA_PELANGGAN   =  data.getString(GlobalConfig.PNAMA_PELANGGAN);
                                    i_data.ALAMAT_PELANGGAN =  data.getString(GlobalConfig.PALAMAT_PELANGGAN);
                                    i_data.PEMBAYARAN       =  data.getString(GlobalConfig.PPEMBAYARAN);
                                    i_data.NO_KUITANSI      =  data.getString(GlobalConfig.PNO_KUITANSI);
                                    i_data.TGL_KUITANSI     =  data.getString(GlobalConfig.PTGL_KUITANSI);
                                    i_data.NO_FAKTUR        =  data.getString(GlobalConfig.PNO_FAKTUR);
                                    i_data.TGL_FAKTUR       =  data.getString(GlobalConfig.PTGL_FAKTUR);
                                    i_data.TOTAL            =  data.getString(GlobalConfig.PTOTAL);

                                    list_datas.add(i_data);
                                    adapter.notifyDataSetChanged();
                                    swipeContainer.setRefreshing(false);
                                }
                            }else{
                                notifikasi("Data tidak ditemukan.");
                                swipeContainer.setRefreshing(false);
                            }
                        }else{
                            notifikasi(message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        swipeContainer.setRefreshing(false);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // do something
                    swipeContainer.setRefreshing(false);
                    NetworkResponse response = error.networkResponse;
                    if(response != null && response.data != null){
                        myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "code"+response.statusCode);
                        switch(response.statusCode){
                            case 404:
                                notifikasi("Terjadi masalah dengan server.");
                                break;
                            case 408:
                                notifikasi("Waktu terlalu lama untuk memproses, silakan ulangi lagi!");
                                break;
                            case 500:
                                notifikasi("Terjadi masalah dengan server.");
                                break;
                            default:
                                notifikasi("Mohon maaf terjadi kesalahan.");
                                break;
                        }
                    }
                }
            }){
                public Map<String, String> getHeaders() {
                    Map<String,String> headers = new Hashtable<String, String>();

                    //Adding parameters
                    headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }};

            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            requestQueue.add(request);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    private void notifikasi(String message){
        Snackbar snack = Snackbar.make(ll_main, message, Snackbar.LENGTH_LONG);
        snack.setActionTextColor(getResources().getColor(android.R.color.white )).show();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        //getMenuInflater().inflate(R.menu.menu_pelanggaran_santri, menu);
        return true;
    }
    private void cancelRequest(){
        if(request!=null) {
            requestQueue.cancelAll(request);
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelRequest();
    }
}