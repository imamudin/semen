package com.muslimtekno.bigsales;

/**
 * Created by imamudin on 12/01/17.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.muslimtekno.bigsales.app.MyAppController;
import com.muslimtekno.bigsales.config.GlobalConfig;
import com.muslimtekno.bigsales.mysp.ObscuredSharedPreferences;
import com.muslimtekno.bigsales.util.MyLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

public class InputStok extends AppCompatActivity {
    LinearLayout ll_main, ll_stok;
    Button btn_simpan;
    TextView tv_nama_toko, tv_poin, tv_alamat, tv_piutang, tv_jatuh_tempo;
    ObscuredSharedPreferences pref;
    ProgressDialog loading;
    JsonObjectRequest request;
    MyLog myLog;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.input_stok);

        init();
    }
    private void init(){
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Input Stok");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(false);

        pref = new ObscuredSharedPreferences(InputStok.this,
                this.getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE));
        myLog = new MyLog(getLocalClassName());

        ll_main         = (LinearLayout)findViewById(R.id.ll_main);
        ll_stok         = (LinearLayout)findViewById(R.id.ll_stok);
        btn_simpan      = (Button)findViewById(R.id.btn_simpan);

        tv_alamat       = (TextView)findViewById(R.id.tv_alamat);
        tv_nama_toko    = (TextView)findViewById(R.id.tv_nama_toko);
        tv_piutang      = (TextView)findViewById(R.id.tv_piutang);
        tv_poin         = (TextView)findViewById(R.id.tv_poin);
        tv_jatuh_tempo  = (TextView)findViewById(R.id.tv_jatuh_tempo);

        tv_nama_toko.setText(pref.getString(GlobalConfig.GTOKO_NAMA,""));
        tv_alamat.setText(pref.getString(GlobalConfig.GTOKO_ALAMAT,""));
        tv_poin.setText(pref.getString(GlobalConfig.GTOKO_POIN,""));
        tv_piutang.setText(decimalToRupiah(Double.parseDouble(pref.getString(GlobalConfig.GTOKO_TOTAL_PIUTANG_KAP,"0"))));
        tv_jatuh_tempo.setText(pref.getString(GlobalConfig.GTOKO_TGL_JATUH_TEMPO,""));

        Intent old  = getIntent();
        String jsonProduk    = old.getStringExtra(GlobalConfig.GPRODUK);
        try{
            JSONArray ja_produk = new JSONArray(jsonProduk);
            for(int i=0; i<ja_produk.length(); i++){
                JSONObject jo_produk = ja_produk.getJSONObject(i);
                add_form_stok(jo_produk.getString(GlobalConfig.GPRODUK_ID), jo_produk.getString(GlobalConfig.GPRODUK_NAMA));
            }
        }catch (Exception e){
            myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "Gagal memproses json produk."+e.toString());
        }

        btn_simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mendapatkan data semua stok
                boolean is_lebih = false;
                final ArrayList<String> id_produk = new ArrayList<String>();
                final ArrayList<Integer> jumlah = new ArrayList<Integer>();
                ArrayList<String> nama = new ArrayList<String>();
                for(int i=0;i<ll_stok.getChildCount();i++){
                    LinearLayout child = (LinearLayout)ll_stok.getChildAt(i);

                    TextView tv_id     = (TextView)child.getChildAt(1);
                    TextView tv_nama   = (TextView)child.getChildAt(2);
                    EditText et_jumlah = (EditText)child.getChildAt(3);

                    String s_jumlah = et_jumlah.getText().toString();
                    String s_id     = tv_id.getText().toString();
                    String s_nama   = tv_nama.getText().toString();

                    if(s_jumlah.equals("")){
                        s_jumlah="0";
                    }
                    if(Integer.parseInt(s_jumlah)>GlobalConfig.MAX_STOK){
                        is_lebih = true;
                    }
                    //masukan ke arraylist semua walaupun nilai 0;
                    id_produk.add(s_id);
                    jumlah.add(Integer.parseInt(s_jumlah));
                    nama.add(s_nama);
                    //myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), s_id+" | "+Integer.parseInt(s_jumlah));
                }
                if(is_lebih){
                    notifikasi_dialog(getResources().getString(R.string.app_name), "Jumlah maksimal stok "+GlobalConfig.MAX_STOK);
                }else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(InputStok.this);
                    View view = InputStok.this.getLayoutInflater().inflate(R.layout.dialog_stok, null);
                    LinearLayout ll_stok_dialog = (LinearLayout)view.findViewById(R.id.ll_stok_dialog);

                    for(int j=0; j<id_produk.size();j++){
                        add_list_stok(ll_stok_dialog, nama.get(j), jumlah.get(j));
                        //myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(),id_produk.get(j)+"  "+jumlah.get(j));
                    }
                    builder.setTitle("Data Stok")
                            .setCancelable(false)
                            .setView(view)
                            .setPositiveButton("Kirim", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    simpanStok(id_produk, jumlah);
                                    //Toast.makeText(getApplicationContext(), "kirim", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }
        });
    }
    private void add_list_stok(final LinearLayout ll_stok, final String nama, final Integer jumlah){
        LayoutInflater inflater     = (LayoutInflater)this.getLayoutInflater();
        View stok_view    = inflater.inflate(R.layout.list_dialog_stok, null);

        final TextView tv_title =(TextView)stok_view.findViewById(R.id.tv_produk);
        final TextView tv_jumlah=(TextView)stok_view.findViewById(R.id.tv_jumlah_stok);

        tv_title.setText(""+nama);
        tv_jumlah.setText(""+jumlah);

        ll_stok.addView(stok_view);
    }
    private void add_form_stok(String id, String nama){
        LayoutInflater inflater     = (LayoutInflater)this.getLayoutInflater();
        View stok_view    = inflater.inflate(R.layout.list_form_stok, null);

        final TextView tv_id    =(TextView)stok_view.findViewById(R.id.tv_id_produk);
        final TextView tv_title =(TextView)stok_view.findViewById(R.id.tv_title);
        final EditText et_jumlah=(EditText)stok_view.findViewById(R.id.et_total);

        tv_title.setText(nama);
        tv_id.setText(id);

        ll_stok.addView(stok_view);
    }
    private String decimalToRupiah(double harga){
        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        kursIndonesia.setDecimalFormatSymbols(formatRp);

        String hasil = kursIndonesia.format(harga);
        return hasil.substring(0, (hasil.length()-3));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //menu.add(1, 2, 1, "Tambah Stok").setIcon(R.drawable.ic_add).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }
    private void simpanStok(ArrayList<String> id_produk, ArrayList<Integer> jumlah){
        myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "make progress dialog");
        loading = ProgressDialog.show(InputStok.this, "Menyimpan data stok.", "Mohon tunggu...", true);
        String url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_INPUTSTOK;

        myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(),""+url);
        JSONObject jsonBody;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(GlobalConfig.USER_ID, pref.getString(GlobalConfig.USER_ID, ""));       //mewaliki nik juga
            jsonBody.put(GlobalConfig.USER_TOKEN, pref.getString(GlobalConfig.USER_TOKEN, ""));

            jsonBody.put(GlobalConfig.ID_TOKO, pref.getString(GlobalConfig.ID_TOKO, ""));
            final JSONArray json_id_produk  = new JSONArray();
            final JSONArray json_jumlah     = new JSONArray();
            for(int i=0; i<id_produk.size();i++){
                json_id_produk.put(i, ""+id_produk.get(i));
                json_jumlah.put(i, ""+jumlah.get(i));
            }
            jsonBody.put(GlobalConfig.GPRODUK_ID, json_id_produk);
            jsonBody.put(GlobalConfig.GSTOK_TOKO, json_jumlah);
            myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), jsonBody.toString());
            request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loading.dismiss();
                    myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "tutup loading");
                    try {
                        int status      = response.getInt("status");
                        String message  = response.getString("message");
                        if(status==1){
                            dialogNotifikasi("Stok berhasil disimpan.");
                        }else{
                            notifikasi(message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //myLog.print("respons",response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loading.dismiss();
                    NetworkResponse response = error.networkResponse;
                    if(response != null && response.data != null){
                        myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "code"+response.statusCode);
                        switch(response.statusCode){
                            case 404:
                                displayMessage("Terjadi masalah dengan server.");
                                break;
                            case 408:
                                displayMessage("Waktu terlalu lama untuk memproses, silakan ulangi lagi!");
                                break;
                            case 500:
                                displayMessage("Terjadi masalah dengan server.");
                                break;
                            default:
                                displayMessage("Mohon maaf terjadi kesalahan.");
                                break;
                        }
                    }
                }
            }){
            public Map<String, String> getHeaders() {
                Map<String,String> headers = new Hashtable<String, String>();

                //Adding parameters
                headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }};
            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MyAppController.getInstance().addToRequestQueue(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void displayMessage(String toastString){
        Toast.makeText(getApplicationContext(), toastString, Toast.LENGTH_LONG).show();
    }
    public void dialogNotifikasi(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(InputStok.this);
        // Set the dialog title
        builder.setTitle(getResources().getString(R.string.app_name))
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Intent toko = new Intent(InputStok.this, Toko.class);
                        startActivity(toko);
                        InputStok.this.finish();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
//            case 1:
//                Toast.makeText(getApplicationContext(), "Input Stok", Toast.LENGTH_SHORT).show();
//                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
            case 2:
                setResult(RESULT_OK);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void notifikasi(String message){
        Snackbar snack = Snackbar.make(ll_main, message, Snackbar.LENGTH_LONG);
        snack.setActionTextColor(getResources().getColor(android.R.color.white )).show();
    }
    private void notifikasi_dialog(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(InputStok.this);
        builder.setTitle(title)
                .setCancelable(false)
                .setMessage(message)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelRequest();
    }
    private void cancelRequest(){
        if(request!=null) {
            MyAppController.getInstance().cancelPendingRequests(request);
        }
    }
}