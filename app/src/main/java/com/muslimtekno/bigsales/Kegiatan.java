package com.muslimtekno.bigsales;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.muslimtekno.bigsales.app.MyAppController;
import com.muslimtekno.bigsales.config.GlobalConfig;
import com.muslimtekno.bigsales.mysp.ObscuredSharedPreferences;
import com.muslimtekno.bigsales.util.MyLog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.Map;

/**
 * Created by imamudin on 20/07/17.
 */
public class Kegiatan extends AppCompatActivity{
    ObscuredSharedPreferences pref;
    JsonObjectRequest request;

    LinearLayout ll_main;
    EditText et_kegiatan;
    Button btn_simpan;
    ProgressDialog loading;

    MyLog myLog;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kegiatan);

        init();
    }
    private void init(){
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("KEGIATAN");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(false);

        pref = new ObscuredSharedPreferences(Kegiatan.this,
                this.getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );
        myLog = new MyLog(getLocalClassName());
        
        ll_main         = (LinearLayout)findViewById(R.id.ll_main);

        et_kegiatan     = (EditText) findViewById(R.id.et_kegiatan);
        btn_simpan      = (Button)findViewById(R.id.btn_simpan);

        btn_simpan.setOnClickListener(btnClick);
    }
    View.OnClickListener btnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v==btn_simpan){
                String kegiatan = et_kegiatan.getText().toString().trim();
                if(kegiatan.length() <= 0 ){
                    notifikasi("Silakan masukan kegiatan!");
                }else{
                    simpanKegiatan(kegiatan);
                }
            }
        }
    };
    private void simpanKegiatan(String kegiatan){
        Log.d(GlobalConfig.TAG+"/"+getLocalClassName(), "make progress dialog");
        loading = ProgressDialog.show(Kegiatan.this, "Menyimpan kegiatan", "Mohon tunggu...", true);

        String url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_KEGIATAN;
        //Log.d(GlobalConfig.TAG+"/"+getLocalClassName(),""+url);
        JSONObject jsonBody;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(GlobalConfig.USER_ID, pref.getString(GlobalConfig.USER_ID, ""));       //mewaliki nik juga
            jsonBody.put(GlobalConfig.USER_TOKEN, pref.getString(GlobalConfig.USER_TOKEN, ""));

            jsonBody.put(GlobalConfig.ID_TOKO, pref.getString(GlobalConfig.ID_TOKO, ""));
            jsonBody.put(GlobalConfig.GKUNJUNGAN_TGL, pref.getString(GlobalConfig.GKUNJUNGAN_TGL, ""));
            jsonBody.put(GlobalConfig.GKUNJUNGAN_JAM_M, pref.getString(GlobalConfig.GKUNJUNGAN_JAM_M, ""));

            jsonBody.put("KEGIATAN", kegiatan);

            //Log.d(GlobalConfig.TAG+"/"+getLocalClassName(),""+jsonBody.toString());
            request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loading.dismiss();
                    Log.d(GlobalConfig.TAG+"/"+getLocalClassName(), "tutup loading");
                    try {
                        int status      = response.getInt("status");
                        String message  = response.getString("message");
                        if(status==1){
                            notifikasi_dialog("",message);
                        }else{
                            notifikasi(message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //Log.d("respons",response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loading.dismiss();
                    NetworkResponse response = error.networkResponse;
                    if(response != null && response.data != null){
                        Log.d(GlobalConfig.TAG+"/"+getLocalClassName(), "code"+response.statusCode);
                        switch(response.statusCode){
                            case 404:
                                displayMessage("Terjadi masalah dengan server.");
                                break;
                            case 500:
                                displayMessage("Terjadi masalah dengan server.");
                                break;
                            case 408:
                                displayMessage("Waktu terlalu lama untuk memproses, silakan ulangi lagi!");
                                break;
                            default:
                                displayMessage("Mohon maaf terjadi kesalahan.");
                                break;
                        }
                    }
                }
            }){
                public Map<String, String> getHeaders() {
                    Map<String,String> headers = new Hashtable<String, String>();

                    //Adding parameters
                    headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }};

            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MyAppController.getInstance().addToRequestQueue(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }
    public void displayMessage(String toastString){
        Toast.makeText(getApplicationContext(), toastString, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void notifikasi(String message){
        Snackbar snack = Snackbar.make(ll_main, message, Snackbar.LENGTH_LONG);
        snack.setActionTextColor(getResources().getColor(android.R.color.white )).show();
    }

    private void notifikasi_dialog(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(Kegiatan.this);
        builder.setTitle(title)
                .setCancelable(false)
                .setMessage(message)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelRequest();
    }
    private void cancelRequest(){
        if(request!=null) {
            MyAppController.getInstance().cancelPendingRequests(request);
        }
    }
}
