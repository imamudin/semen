package com.muslimtekno.bigsales;

/**
 * Created by agung on 19/02/2016.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.muslimtekno.bigsales.app.MyAppController;
import com.muslimtekno.bigsales.config.GlobalConfig;
import com.muslimtekno.bigsales.mysp.ObscuredSharedPreferences;
import com.muslimtekno.bigsales.util.MyLog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.Map;

//untuk google cloud messaging

public class Login extends AppCompatActivity {
    Button btn_login;
    EditText et_user_name, et_password;
    String s_user_name, s_password;
    RelativeLayout ll_main;

    ObscuredSharedPreferences pref;
    private static final int ACTION_PLAY_SERVICES_DIALOG = 100;
    ProgressDialog loading;

    MyLog myLog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_form);

        init();

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //cek apakah hp konek internet
                ConnectivityManager cm =
                        (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null &&
                        activeNetwork.isConnectedOrConnecting();
                if(isConnected){
                    //cek email dan passwod tidak boleh kosong
                    s_user_name     = et_user_name.getText().toString().trim();
                    s_password      = et_password.getText().toString().trim();

                    if(s_user_name.length() != 0 && s_password.length() != 0){
                        login();
                    }else{
                        notifikasi(GlobalConfig.notif_form_tidak_kosong);
                    }
                }else{
                    notifikasi(GlobalConfig.notif_butuh_koneksi);
                }
            }
        });
    }
    private void notifikasi(String message){
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
        Snackbar snack = Snackbar.make(ll_main, message, Snackbar.LENGTH_LONG);
        snack.setActionTextColor(getResources().getColor(android.R.color.white )).show();
    }
    @Override
    protected void onDestroy() {
        try {
            if (loading != null && loading.isShowing()) {
                loading.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }
    private void init(){
        btn_login       = (Button)findViewById(R.id.btn_login);
        et_user_name    = (EditText)findViewById(R.id.et_user_name);
        et_password     = (EditText)findViewById(R.id.et_password);

        ll_main         = (RelativeLayout)findViewById(R.id.ll_main);

        pref = new ObscuredSharedPreferences(this,
                this.getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );
        myLog = new MyLog(getLocalClassName());

        if(pref.getBoolean(GlobalConfig.IS_LOGIN, false)){
            openMainActivity();
            myLog.print("login", "buka main");
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_ip:
                showdialog_settingip();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void showdialog_settingip() {           //1 untuk pasang, 2 untuk cabut
        LayoutInflater layoutInflater = (LayoutInflater) getLayoutInflater();
        View promptView = layoutInflater.inflate(R.layout.dialog_ip, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Login.this);
        alertDialogBuilder.setView(promptView);

        final TextView t_title = (TextView) promptView.findViewById(R.id.t_title_dialog);
        final EditText et_ip = (EditText) promptView.findViewById(R.id.et_ip);

        t_title.setText("Masukan Nomor IP");
        //get session on sharepreferences
        String uri = pref.getString(GlobalConfig.IP_KEY,"");
        et_ip.setText(uri);

        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("Simpan",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (et_ip.getText().toString() != "") {
                                    //save ip to shared preferences
                                    pref.edit().putString(GlobalConfig.IP_KEY, et_ip.getText().toString().trim()).commit();

                                    Toast.makeText(Login.this, "IP set : " + et_ip.getText().toString().trim(), Toast.LENGTH_LONG).show();
                                }
                            }
                        })
                .setNegativeButton("Batal",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        ;
        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    private void login(){
        loading = ProgressDialog.show(Login.this, "Login...", "Mohon tunggu...", true);
        String url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_LOGIN;
        myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(),""+url);
        JSONObject jsonBody;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(GlobalConfig.USER_NAME, s_user_name);
            jsonBody.put(GlobalConfig.USER_PASWORD, s_password);

            myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), jsonBody.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loading.dismiss();
                    myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), response.toString());
                    try {
                        int status      = response.getInt("status");
                        String message  = response.getString("message");
                        if(status==1){
                            JSONObject data     = response.getJSONObject("data");
                            JSONObject user     = data.getJSONObject("user");
                            JSONObject data_target   = data.getJSONObject("target");
                            JSONObject target   = data_target.getJSONObject("data");
                            JSONObject waktu   = data_target.getJSONObject("waktu");

                            pref.edit().putString(GlobalConfig.USER_ID, user.getString(GlobalConfig.USER_ID)).commit();
                            pref.edit().putString(GlobalConfig.USER_TOKEN, user.getString(GlobalConfig.USER_TOKEN)).commit();

                            pref.edit().putString(GlobalConfig.T_NAMA_PEGAWAI, target.getString(GlobalConfig.T_NAMA_PEGAWAI)).commit();
                            pref.edit().putString(GlobalConfig.T_JUMLAH_ORDER, target.getString(GlobalConfig.T_JUMLAH_ORDER)).commit();
                            pref.edit().putString(GlobalConfig.T_TOTAL_TARGET, target.getString(GlobalConfig.T_TOTAL_TARGET)).commit();
                            pref.edit().putString(GlobalConfig.T_TOTAL_TRANSAKSI, target.getString(GlobalConfig.T_TOTAL_TRANSAKSI)).commit();
                            pref.edit().putString(GlobalConfig.T_TOTAL_REALISASI, target.getString(GlobalConfig.T_TOTAL_REALISASI)).commit();

                            pref.edit().putString(GlobalConfig.T_BULAN, waktu.getString(GlobalConfig.T_BULAN)).commit();
                            pref.edit().putString(GlobalConfig.T_TAHUN, waktu.getString(GlobalConfig.T_TAHUN)).commit();

                            pref.edit().putBoolean(GlobalConfig.IS_LOGIN, true).commit();
                            openMainActivity();
                        }else{
                            notifikasi(message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //myLog.print("respons",response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loading.dismiss();
                    myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "Volley errorl log"+error.toString());
                    NetworkResponse response = error.networkResponse;
                    if(response != null && response.data != null){
                        myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "code"+response.statusCode);
                        switch(response.statusCode){
                            case 404:
                                displayMessage("Terjadi masalah dengan server.");
                                break;
                            case 408:
                                displayMessage("Waktu terlalu lama untuk memproses, silakan ulangi lagi!");
                                break;
                            default:
                                displayMessage("Mohon maaf terjadi kesalahan.");
                                break;
                        }
                    }
                }
            }){
                public Map<String, String> getHeaders() {
                Map<String,String> headers = new Hashtable<String, String>();

                //Adding parameters
                headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }};
            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MyAppController.getInstance().addToRequestQueue(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public String trimMessage(String json, String key){
        String trimmedString = null;

        try{
            JSONObject obj = new JSONObject(json);
            trimmedString = obj.getString(key);
        } catch(JSONException e){
            e.printStackTrace();
            return null;
        }
        return trimmedString;
    }

    //Somewhere that has access to a context
    public void displayMessage(String toastString){
        Toast.makeText(getApplicationContext(), toastString, Toast.LENGTH_LONG).show();
    }
    private void openMainActivity(){
        Intent dashboard = new Intent(Login.this, Dashboard.class);
        startActivity(dashboard);
        Login.this.finish();
    }
}