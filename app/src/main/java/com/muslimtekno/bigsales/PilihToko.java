package com.muslimtekno.bigsales;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.muslimtekno.bigsales.app.MyAppController;
import com.muslimtekno.bigsales.config.GlobalConfig;
import com.muslimtekno.bigsales.listAdapter.ListAdapterToko;
import com.muslimtekno.bigsales.model.TokoModel;
import com.muslimtekno.bigsales.mysp.ObscuredSharedPreferences;
import com.muslimtekno.bigsales.util.MyLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/**
 * Created by imamudin on 12/03/17.
 */
public class PilihToko extends AppCompatActivity
{
    // Movies json url
    String keyword, keyword_jenis;
    ObscuredSharedPreferences pref;
    MyLog myLog;

    //variabel untuk list view
    private List<TokoModel> PilihTokoList = new ArrayList<TokoModel>();
    private ListView listView;
    private ListAdapterToko adapter;

    private SwipeRefreshLayout swipeContainer;
    JsonObjectRequest request;

    int offSet=0;

    Runnable runnable;
    Boolean disableSwipeDown = false;       //untuk mendisable swipe down list view

    //untuk handler AsyncTask
    protected static final int MSG_REGISTER_WEB_SERVER_SUCCESS = 103;
    protected static final int MSG_REGISTER_WEB_SERVER_FAILURE = 104;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pilih_toko);

        init();
    }
    private void init(){
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Pilih Pelanggan");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(false);

        pref = new ObscuredSharedPreferences(this,
                this.getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );
        myLog = new MyLog(getLocalClassName());

        //untuk list view
        listView = (ListView)findViewById(R.id.custom_list);
        PilihTokoList.clear();
        adapter = new ListAdapterToko(PilihToko.this, PilihTokoList);
        listView.setAdapter(adapter);

        //untuk swipelist
        swipeContainer = (SwipeRefreshLayout)findViewById(R.id.swipeContainer);

        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your code to refresh the list here.
                // Make sure you call swipeContainer.setRefreshing(false)
                // once the network request has completed successfully.
                //fetchTimelineAsync(0);
                PilihTokoList.clear();
                adapter.notifyDataSetChanged();
                callNews(0);
                Toast.makeText(PilihToko.this,"refresh",Toast.LENGTH_LONG).show();
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Get the selected item text from ListView
                Object o = listView.getItemAtPosition(position);
                TokoModel tokoModel = (TokoModel) o;

                pref.edit().putString(GlobalConfig.FILTER_TOKOID, tokoModel.KODE).commit();
                pref.edit().putString(GlobalConfig.FILTER_TOKO_NAMA, tokoModel.NAMA).commit();
                pref.edit().putString(GlobalConfig.FILTER_TOKO_ALAMAT, tokoModel.ALAMAT).commit();

                setResult(RESULT_OK);
                finish();
            }
        });

        swipeContainer.post(new Runnable() {
            @Override
            public void run() {
                swipeContainer.setRefreshing(true);
                PilihTokoList.clear();
                adapter.notifyDataSetChanged();
                callNews(0);
            }
        });

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {

            private int currentVisibleItemCount;
            private int currentScrollState;
            private int currentFirstVisibleItem;
            private int totalItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                this.currentScrollState = scrollState;
                this.isScrollCompleted();
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                this.currentFirstVisibleItem = firstVisibleItem;
                this.currentVisibleItemCount = visibleItemCount;
                this.totalItem = totalItemCount;
            }
            private void isScrollCompleted() {
                if (totalItem - currentFirstVisibleItem == currentVisibleItemCount
                        && this.currentScrollState == SCROLL_STATE_IDLE) {

                    if(!disableSwipeDown) {
                        swipeContainer.setRefreshing(true);
                        handler = new Handler();

                        runnable = new Runnable() {
                            public void run() {
                                callNews(offSet);

                            }
                        };
                        //untuk menerlambatkan 1 detik
                        handler.postDelayed(runnable, 1000);
                    }else{
                        //Toast.makeText(PilihToko.this,"Data telah ditampilkan semua.",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }
    private void callNews(int page){
        swipeContainer.setRefreshing(true);
        // Creating volley request obj
        String url = GlobalConfig.IP+GlobalConfig.WEB_URL+ GlobalConfig.URL_TOKOS;

        //myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), url);
        JSONObject jsonBody;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(GlobalConfig.USER_ID, pref.getString(GlobalConfig.USER_ID, ""));
            jsonBody.put(GlobalConfig.USER_TOKEN, pref.getString(GlobalConfig.USER_TOKEN, ""));

            jsonBody.put(GlobalConfig.UP_START, page);
            jsonBody.put(GlobalConfig.UP_LIMIT, GlobalConfig.MAX_ROW_PER_REQUEST);

            myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(),""+jsonBody.toString());
            request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), ""+response.toString());
                    try {
                        int status  = response.getInt("status");
                        String pesan= response.getString("message");
                        myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), ""+status);
                        if(status==1){
                            JSONObject respon_data  = response.getJSONObject("data");
                            int total               = respon_data.getInt("total");

                            if(total<GlobalConfig.MAX_ROW_PER_REQUEST || total==0) {
                                disableSwipeDown = true;
                            }
                            offSet += total;
                            JSONArray datas = respon_data.getJSONArray("toko");

                            if(datas.length()>0) {
                                for (int i = 0; i < datas.length(); i++) {
                                    JSONObject data = datas.getJSONObject(i);

                                    TokoModel tokoModel = new TokoModel(data.getString(GlobalConfig.GT_KODE),data.getString(GlobalConfig.GT_NAMA),
                                            data.getString(GlobalConfig.GT_ALAMAT));

                                    // adding news to news array
                                    PilihTokoList.add(tokoModel);
                                    swipeContainer.setRefreshing(false);

                                    adapter.notifyDataSetChanged();
                                }
                            }else{
                                Toast.makeText(getApplicationContext(),"Toko tidak ditemukan.", Toast.LENGTH_SHORT).show();
                                swipeContainer.setRefreshing(false);
                            }
                        }else{
                            swipeContainer.setRefreshing(false);
                            Toast.makeText(getApplicationContext(),pesan , Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        swipeContainer.setRefreshing(false);
                    }
                    //myLog.print("respons",response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // do something
                    swipeContainer.setRefreshing(false);
                    //myLog.print("respons", error.getMessage());
                }
            }){
                public Map<String, String> getHeaders() {
                    Map<String,String> headers = new Hashtable<String, String>();

                    //Adding parameters
                    headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }};

            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            //Creating a Request Queue
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            //Adding request to the queue
            // Adding request to request queue
            MyAppController.getInstance().addToRequestQueue(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case MSG_REGISTER_WEB_SERVER_SUCCESS:
                    Toast.makeText(getApplicationContext(),
                            "Tidak mendapatkan respon.", Toast.LENGTH_LONG).show();
                    break;
                case MSG_REGISTER_WEB_SERVER_FAILURE:
                    Toast.makeText(getApplicationContext(),
                            "Tidak dapat mengakses server.",
                            Toast.LENGTH_LONG).show();
                    break;
            }
        }
    };
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(1, 1, 1, "Semua").setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                //pilih semua toko dengan manghapus PREF
                pref.edit().putString(GlobalConfig.FILTER_TOKOID, "").commit();
                pref.edit().putString(GlobalConfig.FILTER_TOKO_NAMA, "").commit();
                pref.edit().putString(GlobalConfig.FILTER_TOKO_ALAMAT, "").commit();

                setResult(RESULT_OK);
                finish();
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelRequest();
    }
    private void cancelRequest(){
        if(request!=null) {
            MyAppController.getInstance().cancelPendingRequests(request);
        }
    }
}
