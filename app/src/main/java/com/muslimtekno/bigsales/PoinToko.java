package com.muslimtekno.bigsales;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.JsonObjectRequest;
import com.muslimtekno.bigsales.app.MyAppController;
import com.muslimtekno.bigsales.config.GlobalConfig;
import com.muslimtekno.bigsales.model.Poin;
import com.muslimtekno.bigsales.mysp.ObscuredSharedPreferences;
import com.muslimtekno.bigsales.util.MyLog;

/**
 * Created by imamudin on 02/04/17.
 */

public class PoinToko extends AppCompatActivity {
    LinearLayout ll_main;
    Button btn_poin, btn_tukar;
    TextView tv_nama_toko, tv_total_poin, tv_poin, tv_alamat, tv_jumlah_order, tv_jumlah_beli, tv_tukar;
    ObscuredSharedPreferences pref;
    ProgressDialog loading;
    JsonObjectRequest request;
    Poin poin;

    MyLog myLog;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.poin);

        init();
    }
    private void init(){
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Poin Toko");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(false);

        pref = new ObscuredSharedPreferences(PoinToko.this,
                this.getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );
        myLog = new MyLog(getLocalClassName());

        btn_poin        = (Button)findViewById(R.id.btn_poin);
        btn_tukar       = (Button)findViewById(R.id.btn_tukar);

        tv_nama_toko    = (TextView)findViewById(R.id.tv_nama_toko);
        tv_total_poin   = (TextView)findViewById(R.id.tv_total_poin);
        tv_poin         = (TextView)findViewById(R.id.tv_poin);
        tv_alamat       = (TextView)findViewById(R.id.tv_alamat);
        tv_jumlah_order = (TextView)findViewById(R.id.tv_jumlah_order);
        tv_jumlah_beli  = (TextView)findViewById(R.id.tv_jumlah_beli);
        tv_tukar        = (TextView)findViewById(R.id.tv_tukar);

        Intent old = getIntent();

        String data_old = old.getStringExtra("data");

        poin = new Poin(data_old);

        tv_nama_toko.setText(poin.NAMA);
        tv_total_poin.setText(poin.TOTAL_POIN);
        tv_poin.setText(poin.POIN);
        tv_alamat.setText(poin.ALAMAT);
        tv_jumlah_order.setText(poin.JUMLAH_ORDER+" kali");
        tv_jumlah_beli.setText(poin.JUMLAH_BELI+ " sak");
        tv_tukar.setText(poin.TUKAR);

        btn_poin.setOnClickListener(btnClick);
        btn_tukar.setOnClickListener(btnClick);

        ll_main     = (LinearLayout)findViewById(R.id.ll_main);
    }

    View.OnClickListener btnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent open = null;
            if(v==btn_poin){
                open = new Intent(PoinToko.this, DetailPoinToko.class);
                open.putExtra(GlobalConfig.UP_KODE, poin.KODE);
            }else if(v==btn_tukar){
                open = new Intent(PoinToko.this, DetailPoinTukar.class);
                open.putExtra(GlobalConfig.UP_KODE, poin.KODE);
            }

            if(open != null){
                startActivity(open);
            }else{
                notifikasi("Mohon maaf terjadi kesalahan.");
            }
        }
    };
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //menu.add(1, 2, 1, "Hapus Filter").setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void notifikasi(String message){
        Snackbar snack = Snackbar.make(ll_main, message, Snackbar.LENGTH_LONG);
        snack.setActionTextColor(getResources().getColor(android.R.color.white )).show();
    }
    private void notifikasi_dialog(String title, String message){
        if(title.equals("")){
            title = getResources().getString(R.string.app_name);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(PoinToko.this);
        builder.setTitle(title)
                .setCancelable(false)
                .setMessage(message)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    public void displayMessage(String toastString){
        Toast.makeText(getApplicationContext(), toastString, Toast.LENGTH_LONG).show();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelRequest();
    }
    private void cancelRequest(){
        if(request!=null) {
            MyAppController.getInstance().cancelPendingRequests(request);
        }
    }
    public void openDashboard(){
        Intent dashboard = new Intent (PoinToko.this, Dashboard.class);
        startActivity(dashboard);
        PoinToko.this.finish();
    }
}
