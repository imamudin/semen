package com.muslimtekno.bigsales;

/**
 * Created by imamudin on 12/01/17.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.muslimtekno.bigsales.app.MyAppController;
import com.muslimtekno.bigsales.config.GlobalConfig;
import com.muslimtekno.bigsales.mysp.ObscuredSharedPreferences;
import com.muslimtekno.bigsales.util.MyLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Map;

public class Toko extends AppCompatActivity {
    LinearLayout ll_main;
    Button btn_inputStok, btn_order, btn_selesai, btn_pembayaran, btn_kegiatan;
    TextView tv_nama_toko, tv_poin, tv_alamat, tv_piutang, tv_jatuh_tempo, tv_realplafon, tv_tagihan_terdekat;
    ObscuredSharedPreferences pref;
    ProgressDialog loading;
    JsonObjectRequest request;
    AlertDialog alert_detail_piutang;
    MyLog myLog;
    Button btn_order_non_semen;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.toko);

        init();
    }
    private void init(){
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Data Pelanggan");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(false);

        pref = new ObscuredSharedPreferences(Toko.this,
                this.getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );
        myLog = new MyLog(getLocalClassName());

        btn_inputStok   = (Button)findViewById(R.id.btn_input_stok);
        btn_order       = (Button)findViewById(R.id.btn_order);
        btn_selesai     = (Button)findViewById(R.id.btn_selesai);
        btn_pembayaran  = (Button)findViewById(R.id.btn_pembayaran);
        btn_kegiatan    = (Button)findViewById(R.id.btn_kegiatan);

        tv_alamat       = (TextView)findViewById(R.id.tv_alamat);
        tv_nama_toko    = (TextView)findViewById(R.id.tv_nama_toko);
        tv_piutang      = (TextView)findViewById(R.id.tv_piutang);
        tv_poin         = (TextView)findViewById(R.id.tv_poin);
        tv_jatuh_tempo  = (TextView)findViewById(R.id.tv_jatuh_tempo);
        tv_realplafon   = (TextView)findViewById(R.id.tv_jumlah_plafon);
        tv_tagihan_terdekat  = (TextView)findViewById(R.id.tv_tagihan_terdekat);

        tv_nama_toko.setText(pref.getString(GlobalConfig.GTOKO_NAMA,""));
        tv_alamat.setText(pref.getString(GlobalConfig.GTOKO_ALAMAT,""));
        tv_poin.setText(pref.getString(GlobalConfig.GTOKO_POIN,""));
        tv_piutang.setText(decimalToRupiah(Double.parseDouble(pref.getString(GlobalConfig.GTOKO_TOTAL_PIUTANG_KAP,"0"))));
        tv_jatuh_tempo.setText(pref.getString(GlobalConfig.GTOKO_TGL_JATUH_TEMPO,""));
        tv_realplafon.setText(decimalToRupiah(Double.parseDouble(pref.getString(GlobalConfig.GTOKO_REALPLAFON,"0"))));
        tv_tagihan_terdekat.setText(decimalToRupiah(Double.parseDouble(pref.getString(GlobalConfig.GTOKO_TAGIHAN_TERDEKAT,"0"))));


        btn_inputStok.setOnClickListener(btnClick);
        btn_order.setOnClickListener(btnClick);
        btn_selesai.setOnClickListener(btnClick);
        btn_pembayaran.setOnClickListener(btnClick);
        btn_kegiatan.setOnClickListener(btnClick);

        ll_main             = (LinearLayout)findViewById(R.id.ll_main);

        btn_order_non_semen = (Button)findViewById(R.id.btn_order_non_semen);
        btn_order_non_semen.setOnClickListener(btnClick);
    }

    View.OnClickListener btnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent open = null;
            if(v==btn_inputStok){
                mengecekFakturInputStok();
                return;
            }else if(v==btn_order){
                //request harga dulu
                mengecekFaktur();
                return;
                //open = new Intent(TokoModel.this, FormOrder.class);
            }else if(v == btn_selesai){
                onBackPressed();
                return;
            }else if(v == btn_pembayaran){
                getDetailPiutang();
                return;
            }else if(v == btn_kegiatan){
                Intent a = new Intent(Toko.this, Kegiatan.class);
                startActivity(a);
                return;
            }else if(v == btn_order_non_semen){
                mengecekFakturNonSemen();
                return;
            }

            if(open != null){
                startActivity(open);
            }else{
                notifikasi("Mohon maaf terjadi kesalahan.");
            }
        }
    };
    private String decimalToRupiah(double harga){
        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        kursIndonesia.setDecimalFormatSymbols(formatRp);

        String hasil = kursIndonesia.format(harga);
        return hasil.substring(0, (hasil.length()-3));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //menu.add(1, 2, 1, "Hapus Filter").setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void notifikasi(String message){
        Snackbar snack = Snackbar.make(ll_main, message, Snackbar.LENGTH_LONG);
        snack.setActionTextColor(getResources().getColor(android.R.color.white )).show();
    }
    private void notifikasi_dialog(String title, String message){
        if(title.equals("")){
            title = getResources().getString(R.string.app_name);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(Toko.this);
        builder.setTitle(title)
                .setCancelable(false)
                .setMessage(message)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    @Override
    public void onBackPressed()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(Toko.this);
        // Set the dialog title
        builder.setTitle(getResources().getString(R.string.app_name))
                .setMessage("Apakah anda telah selesai melakukan kunjungan?")
                .setCancelable(false)
                .setPositiveButton("Selesai", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        tutupKunjungan();
                    }
                })
                .setNegativeButton("Belum", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    private void mengecekFakturInputStok(){
        myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "make progress dialog");
        loading = ProgressDialog.show(Toko.this, "Menyiapkan Form", "Mohon tunggu...", true);
        String url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_FORMSTOK;
        myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(),""+url);
        JSONObject jsonBody = null;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                loading.dismiss();
                myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "tutup loading");
                try {
                    int status      = response.getInt("status");
                    String message  = response.getString("message");
                    if(status==1){
                        JSONObject data     = response.getJSONObject("data");
                        JSONArray dproduk  = data.getJSONArray("produk");
                        Intent open = new Intent(Toko.this, InputStok.class);
                        open.putExtra(GlobalConfig.GPRODUK, dproduk.toString());
                        startActivity(open);
                    }else{
                        notifikasi(message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //myLog.print("respons",response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
                NetworkResponse response = error.networkResponse;
                if(response != null && response.data != null){
                    myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "code"+response.statusCode);
                    switch(response.statusCode){
                        case 404:
                            displayMessage("Terjadi masalah dengan server.");
                            break;
                        case 500:
                            displayMessage("Terjadi masalah dengan server.");
                            break;
                        case 408:
                            displayMessage("Waktu terlalu lama untuk memproses, silakan ulangi lagi!");
                            break;
                        default:
                            displayMessage("Mohon maaf terjadi kesalahan.");
                            break;
                    }
                }
            }
        }){
            public Map<String, String> getHeaders() {
                Map<String,String> headers = new Hashtable<String, String>();

                //Adding parameters
                headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }};

        request.setRetryPolicy(new DefaultRetryPolicy(
                GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyAppController.getInstance().addToRequestQueue(request);
    }
    private void mengecekFakturNonSemen(){
        loading = ProgressDialog.show(Toko.this, "Mengecek Faktur", "Mohon tunggu...", true);
        String url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_FORMORDER_NON_SEMEN;
        myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(),""+url);
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(GlobalConfig.USER_ID, pref.getString(GlobalConfig.USER_ID, ""));       //mewaliki nik juga
            jsonBody.put(GlobalConfig.USER_TOKEN, pref.getString(GlobalConfig.USER_TOKEN, ""));
            jsonBody.put(GlobalConfig.ID_TOKO, pref.getString(GlobalConfig.ID_TOKO, ""));

            myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), jsonBody.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loading.dismiss();
                    myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "tutup loading");
                    try {
                        int status = response.getInt("status");
                        String message = response.getString("message");
                        if (status == 1) {
                            JSONObject data = response.getJSONObject("data");
                            JSONArray dharga = data.getJSONArray(GlobalConfig.GPRODUK_NON_SEMEN);
                            JSONArray gudang = data.getJSONArray("gudang");
                            String default_gudang = data.getString(GlobalConfig.GDEFAULT_GUDANG);
                            boolean ada_faktur = data.getBoolean(GlobalConfig.GADA_FAKTUR);


                            double piutang = Double.parseDouble(data.getString(GlobalConfig.GTOKO_TOTAL_PIUTANG));
                            final Intent open = new Intent(Toko.this, FormOrderNonSemen.class);
                            open.putExtra(GlobalConfig.GPRODUK_NON_SEMEN, dharga.toString());
                            open.putExtra(GlobalConfig.GGUDANG, gudang.toString());
                            open.putExtra(GlobalConfig.GTOKO_TOTAL_PIUTANG, piutang);
                            open.putExtra(GlobalConfig.GDEFAULT_GUDANG, default_gudang);
                            open.putExtra(GlobalConfig.GADA_FAKTUR, ada_faktur);

                            if(!ada_faktur){     //jika false, maka akan muncul pertanyaan terlebih dahulu
                                message    = "Pelanggan belum melakukan pembayaran piutang sesuai jatuh tempo."+System.getProperty("line.separator")+"Order pelanggan selanjutnya menunggu appoval dari perusahaan. apakah akan melanjutkan proses order ?";
                                AlertDialog.Builder builder = new AlertDialog.Builder(Toko.this);
                                builder.setTitle("")
                                        .setCancelable(false)
                                        .setMessage(message)
                                        .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                                notifikasi_dialog("","Order dibatalkan.");
                                            }
                                        }).setPositiveButton("Lanjutkan", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {
                                        startActivity(open);
                                    }
                                });;
                                AlertDialog dialog = builder.create();
                                dialog.show();
                            }else{              //jika true, maka langsung tanpa pertanyaan
                                startActivity(open);
                            }
                        } else {
                            notifikasi_dialog("",message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //myLog.print("respons", response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loading.dismiss();
                    NetworkResponse response = error.networkResponse;
                    if (response != null && response.data != null) {
                        myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "code" + response.statusCode);
                        switch (response.statusCode) {
                            case 404:
                                displayMessage("Terjadi masalah dengan server.");
                                break;
                            case 500:
                                displayMessage("Terjadi masalah dengan server.");
                                break;
                            case 408:
                                displayMessage("Waktu terlalu lama untuk memproses, silakan ulangi lagi!");
                                break;
                            default:
                                displayMessage("Mohon maaf terjadi kesalahan.");
                                break;
                        }
                    }
                }
            }) {
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new Hashtable<String, String>();

                    //Adding parameters
                    headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MyAppController.getInstance().addToRequestQueue(request);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    private void mengecekFaktur(){
        loading = ProgressDialog.show(Toko.this, "Mengecek Faktur", "Mohon tunggu...", true);
        String url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_FORMORDER;
        myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(),""+url);
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(GlobalConfig.USER_ID, pref.getString(GlobalConfig.USER_ID, ""));       //mewaliki nik juga
            jsonBody.put(GlobalConfig.USER_TOKEN, pref.getString(GlobalConfig.USER_TOKEN, ""));
            jsonBody.put(GlobalConfig.ID_TOKO, pref.getString(GlobalConfig.ID_TOKO, ""));

            myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), jsonBody.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loading.dismiss();
                    myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "tutup loading");
                    try {
                        int status = response.getInt("status");
                        String message = response.getString("message");
                        if (status == 1) {
                            JSONObject data = response.getJSONObject("data");
                            JSONArray dharga = data.getJSONArray("harga");
                            JSONArray gudang = data.getJSONArray("gudang");
                            String default_gudang = data.getString(GlobalConfig.GDEFAULT_GUDANG);
                            boolean ada_faktur = data.getBoolean(GlobalConfig.GADA_FAKTUR);


                            double piutang = Double.parseDouble(data.getString(GlobalConfig.GTOKO_TOTAL_PIUTANG));
                            final Intent open = new Intent(Toko.this, FormOrder.class);
                            open.putExtra(GlobalConfig.GHARGA, dharga.toString());
                            open.putExtra(GlobalConfig.GGUDANG, gudang.toString());
                            open.putExtra(GlobalConfig.GTOKO_TOTAL_PIUTANG, piutang);
                            open.putExtra(GlobalConfig.GDEFAULT_GUDANG, default_gudang);
                            open.putExtra(GlobalConfig.GADA_FAKTUR, ada_faktur);

                            if(!ada_faktur){     //jika false, maka akan muncul pertanyaan terlebih dahulu
                                message    = "Pelanggan belum melakukan pembayaran piutang sesuai jatuh tempo."+System.getProperty("line.separator")+"Order pelanggan selanjutnya menunggu appoval dari perusahaan. apakah akan melanjutkan proses order ?";
                                AlertDialog.Builder builder = new AlertDialog.Builder(Toko.this);
                                builder.setTitle("")
                                        .setCancelable(false)
                                        .setMessage(message)
                                        .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                                notifikasi_dialog("","Order dibatalkan.");
                                            }
                                        }).setPositiveButton("Lanjutkan", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {
                                        startActivity(open);
                                    }
                                });;
                                AlertDialog dialog = builder.create();
                                dialog.show();
                            }else{              //jika true, maka langsung tanpa pertanyaan
                                startActivity(open);
                            }
                        } else {
                            notifikasi_dialog("",message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //myLog.print("respons", response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loading.dismiss();
                    NetworkResponse response = error.networkResponse;
                    if (response != null && response.data != null) {
                        myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "code" + response.statusCode);
                        switch (response.statusCode) {
                            case 404:
                                displayMessage("Terjadi masalah dengan server.");
                                break;
                            case 500:
                                displayMessage("Terjadi masalah dengan server.");
                                break;
                            case 408:
                                displayMessage("Waktu terlalu lama untuk memproses, silakan ulangi lagi!");
                                break;
                            default:
                                displayMessage("Mohon maaf terjadi kesalahan.");
                                break;
                        }
                    }
                }
            }) {
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new Hashtable<String, String>();

                    //Adding parameters
                    headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MyAppController.getInstance().addToRequestQueue(request);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    private void tutupKunjungan(){
        myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "make progress dialog");
        loading = ProgressDialog.show(Toko.this, "Menutup Kunjungan", "Mohon tunggu...", true);

        String url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_TUTUPKUNJUNGAN;
        //myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(),""+url);
        JSONObject jsonBody;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(GlobalConfig.USER_ID, pref.getString(GlobalConfig.USER_ID, ""));       //mewaliki nik juga
            jsonBody.put(GlobalConfig.USER_TOKEN, pref.getString(GlobalConfig.USER_TOKEN, ""));

            jsonBody.put(GlobalConfig.ID_TOKO, pref.getString(GlobalConfig.ID_TOKO, ""));
            jsonBody.put(GlobalConfig.GKUNJUNGAN_TGL, pref.getString(GlobalConfig.GKUNJUNGAN_TGL, ""));
            jsonBody.put(GlobalConfig.GKUNJUNGAN_JAM_M, pref.getString(GlobalConfig.GKUNJUNGAN_JAM_M, ""));

            //myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(),""+jsonBody.toString());
            request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loading.dismiss();
                    myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "tutup loading");
                    try {
                        int status      = response.getInt("status");
                        String message  = response.getString("message");
                        if(status==1){
                            //hapus semua preferences
                            //update database dulu, kemudian baru keluar
                            pref.edit().remove(GlobalConfig.IS_TOKO).commit();
                            pref.edit().remove(GlobalConfig.ID_TOKO).commit();
                            pref.edit().remove(GlobalConfig.GTOKO_NAMA).commit();
                            pref.edit().remove(GlobalConfig.GTOKO_ALAMAT).commit();
                            pref.edit().remove(GlobalConfig.GTOKO_PEMILIK).commit();
                            pref.edit().remove(GlobalConfig.GTOKO_NOTELP).commit();
                            pref.edit().remove(GlobalConfig.GREAL_PLAFON).commit();
                            pref.edit().remove(GlobalConfig.GKDSALES).commit();
                            pref.edit().remove(GlobalConfig.GJATUHTEMPOBAYAR).commit();
                            pref.edit().remove(GlobalConfig.GTOKO_LAT).commit();
                            pref.edit().remove(GlobalConfig.GTOKO_LONG).commit();
                            pref.edit().remove(GlobalConfig.GTOKO_NIK).commit();
                            pref.edit().remove(GlobalConfig.GTOKO_POIN).commit();
                            pref.edit().remove(GlobalConfig.ID_TOKO).commit();

                            pref.edit().remove(GlobalConfig.GTOKO_TOTAL_PIUTANG_KAP).commit();
                            pref.edit().remove(GlobalConfig.GTOKO_TGL_JATUH_TEMPO).commit();
                            pref.edit().remove(GlobalConfig.GTOKO_REALPLAFON).commit();
                            pref.edit().remove(GlobalConfig.GTOKO_TAGIHAN_TERDEKAT).commit();

                            pref.edit().remove(GlobalConfig.GKUNJUNGAN_TGL).commit();
                            pref.edit().remove(GlobalConfig.GKUNJUNGAN_JAM_M).commit();
                            tutupActivity();
                        }else{
                            notifikasi(message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //myLog.print("respons",response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loading.dismiss();
                    NetworkResponse response = error.networkResponse;
                    if(response != null && response.data != null){
                        myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "code"+response.statusCode);
                        switch(response.statusCode){
                            case 404:
                                displayMessage("Terjadi masalah dengan server.");
                                break;
                            case 500:
                                displayMessage("Terjadi masalah dengan server.");
                                break;
                            case 408:
                                displayMessage("Waktu terlalu lama untuk memproses, silakan ulangi lagi!");
                                break;
                            default:
                                displayMessage("Mohon maaf terjadi kesalahan.");
                                break;
                        }
                    }
                }
            }){
                public Map<String, String> getHeaders() {
                    Map<String,String> headers = new Hashtable<String, String>();

                    //Adding parameters
                    headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }};

            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MyAppController.getInstance().addToRequestQueue(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void tutupActivity(){
        AlertDialog.Builder builder = new AlertDialog.Builder(Toko.this);
        // Set the dialog title
        builder.setTitle(getResources().getString(R.string.app_name))
                .setMessage("Kunjungan telah selesai.\nTerima kasih.")
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        openDashboard();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    public void displayMessage(String toastString){
        Toast.makeText(getApplicationContext(), toastString, Toast.LENGTH_LONG).show();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelRequest();
    }
    private void cancelRequest(){
        if(request!=null) {
            MyAppController.getInstance().cancelPendingRequests(request);
        }
    }
    public void openDashboard(){
        Toko.this.finish();
    }
    private void getDetailPiutang(){
        String url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_PELUNASAN_TOKO;
        myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(),""+url);

        loading = ProgressDialog.show(Toko.this, "", "Mohon tunggu...", true);
        //bisa order
        JSONObject jsonBody;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(GlobalConfig.USER_ID, pref.getString(GlobalConfig.USER_ID, ""));
            jsonBody.put(GlobalConfig.USER_TOKEN, pref.getString(GlobalConfig.USER_TOKEN, ""));

            jsonBody.put(GlobalConfig.ID_TOKO, pref.getString(GlobalConfig.ID_TOKO, ""));
            myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), jsonBody.toString());
            request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loading.dismiss();
                    myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), response.toString());
                    try {
                        int status      = response.getInt("status");
                        String message  = response.getString("message");
                        if(status==1){
                            JSONObject datas = response.getJSONObject("data");
                            showDetailPiutang(datas.toString());
                        }else{
                            notifikasi(message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //myLog.print("respons",response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loading.dismiss();
                    NetworkResponse response = error.networkResponse;
                    if(response != null && response.data != null){
                        myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "code"+response.statusCode);
                        switch(response.statusCode){
                            case 404:
                                notifikasi("Terjadi masalah dengan server.");
                                break;
                            case 408:
                                notifikasi("Waktu terlalu lama untuk memproses, silakan ulangi lagi!");
                                break;
                            case 500:
                                notifikasi("Terjadi masalah dengan server.");
                                break;
                            default:
                                notifikasi("Mohon maaf terjadi kesalahan.");
                                break;
                        }
                    }
                }
            }){
                public Map<String, String> getHeaders() {
                    Map<String,String> headers = new Hashtable<String, String>();

                    //Adding parameters
                    headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }};

            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MyAppController.getInstance().addToRequestQueue(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void showDetailPiutang(String dataString){
        LayoutInflater layoutInflater = (LayoutInflater)getLayoutInflater();
        View promptView = layoutInflater.inflate(R.layout.notif_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Toko.this);
        alertDialogBuilder.setView(promptView);

        final TextView t_title      = (TextView) promptView.findViewById(R.id.t_title_dialog);
        final LinearLayout ll_notif = (LinearLayout) promptView.findViewById(R.id.ll_notif);

        t_title.setText(pref.getString(GlobalConfig.FILTER_TOKO_NAMA,""));
        try {
            //myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), dataString);
            JSONObject data = new JSONObject(dataString);
            JSONArray datas = data.getJSONArray("piutang");
            for(int i=0;i<datas.length();i++){
                JSONObject piutang  = datas.getJSONObject(i);

                String faktur       = piutang.getString(GlobalConfig.GDP_NO_FAKTUR);
                String tanggal_faktur= piutang.getString(GlobalConfig.GDP_TGL_FAKTUR);
                String total        = piutang.getString(GlobalConfig.GDP_TOTAL);
                String jatuh_tempo  = piutang.getString(GlobalConfig.GDP_JATUH_TEMPO);
                String belum_dibayar= piutang.getString(GlobalConfig.GDP_BELUM_DIBAYAR);

                if(total.length()<=0) total ="0";

                ll_notif.addView(getViewDataPiutang(i,faktur, tanggal_faktur, decimalToRupiah(Double.parseDouble(total)), jatuh_tempo, Double.parseDouble(belum_dibayar)));
            }
        }catch (Exception e){
            //myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), e.getMessage().toString());
            myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "Tidak dapat memproses data json.");
        }
        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setNegativeButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
    private View getViewDataPiutang(int urutan, final String faktur, String tanggal_faktur, final String total, String jatuh_tempo, final double belum_dibayar){
        LayoutInflater inflater     = (LayoutInflater)this.getLayoutInflater();
        View data_history_view    = inflater.inflate(R.layout.list_piutang_dialog_button, null);

        final TextView tv_faktur        =(TextView)data_history_view.findViewById(R.id.tv_faktur);
        final TextView tv_tanggal_faktur=(TextView)data_history_view.findViewById(R.id.tv_tanggal_faktur);
        final TextView tv_total         =(TextView)data_history_view.findViewById(R.id.tv_total);
        final TextView tv_jatuh_tempo   =(TextView)data_history_view.findViewById(R.id.tv_tanggal_jatuh_tempo);
        final TextView tv_belum_dibayar =(TextView)data_history_view.findViewById(R.id.tv_belum_dibayar);
        final Button btn_bayar          = (Button) data_history_view.findViewById(R.id.btn_bayar);

        tv_faktur.setText(faktur);
        tv_tanggal_faktur.setText(tanggal_faktur);
        tv_total.setText(total);
        tv_jatuh_tempo.setText(jatuh_tempo);
        if(urutan!=0){
            btn_bayar.setVisibility(View.GONE);
        }
        tv_belum_dibayar.setText(decimalToRupiah(belum_dibayar));
        btn_bayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater layoutInflater = (LayoutInflater)getLayoutInflater();
                View promptView = layoutInflater.inflate(R.layout.dialog_pembayaran, null);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Toko.this);
                alertDialogBuilder.setView(promptView);

                final TextView t_title      = (TextView) promptView.findViewById(R.id.t_title_dialog);
                final TextView tv_jumlah_pembayaran      = (TextView) promptView.findViewById(R.id.et_jumlah_pembayaran);
                final TextView tv_tgl_bayar = (TextView) promptView.findViewById(R.id.tv_tanggal_pembayaran);
                final TextView tv_tgl_bayar_kirim   = (TextView) promptView.findViewById(R.id.tv_tanggal_pembayaran_kirim);
                final TextView tv_faktur_pembayaran = (TextView) promptView.findViewById(R.id.tv_faktur_pembayaran);
                final LinearLayout ll_tgl_pembayaran= (LinearLayout) promptView.findViewById(R.id.ll_tgl_pembayaran);

                t_title.setText(pref.getString(GlobalConfig.FILTER_TOKO_NAMA,""));
                tv_faktur_pembayaran.setText(faktur);
                tv_jumlah_pembayaran.setText(""+(int)(belum_dibayar));
                Calendar c = Calendar.getInstance();
                SimpleDateFormat format_s   = new SimpleDateFormat("dd MMM yyyy");
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

                tv_tgl_bayar.setText(""+format_s.format(c.getTime()));
                tv_tgl_bayar_kirim.setText(""+format.format(c.getTime()));

                ll_tgl_pembayaran.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final View dialogView = View.inflate(Toko.this, R.layout.date_picker, null);
                        final AlertDialog alertDialog = new AlertDialog.Builder(Toko.this).create();
                        final DatePicker dp_awal = (DatePicker) dialogView.findViewById(R.id.date_picker);
                        dp_awal.setMinDate(System.currentTimeMillis());

                        dialogView.findViewById(R.id.date_time_set).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Calendar cal = new GregorianCalendar(dp_awal.getYear(),
                                        dp_awal.getMonth(),
                                        dp_awal.getDayOfMonth());

                                SimpleDateFormat format_show = new SimpleDateFormat("dd MMM yyyy");
                                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                                Log.i(GlobalConfig.TAG+"/"+getLocalClassName(), "tanggal : "+format_show.format(cal.getTime()));
                                tv_tgl_bayar.setText(format_show.format(cal.getTime()));
                                tv_tgl_bayar_kirim.setText(format.format(cal.getTime()));

                                alertDialog.dismiss();
                            }});
                        alertDialog.setView(dialogView);
                        alertDialog.show();
                    }
                });

                alertDialogBuilder.setCancelable(false)
                        .setPositiveButton("BAYAR",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //myLog.print(getLocalClassName(), tv_jumlah_pembayaran.getText().toString()+" | "+tv_faktur.getText().toString());
                                        bayarPiutang(Integer.parseInt(tv_jumlah_pembayaran.getText().toString()), tv_tgl_bayar_kirim.getText().toString(), tv_faktur_pembayaran.getText().toString() );
                                        dialog.cancel();
                                    }
                                })
                        .setNegativeButton("BATAL",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                // create an alert dialog
                alert_detail_piutang = alertDialogBuilder.create();
                alert_detail_piutang.show();
            }
        });

        return data_history_view;
    }
    private void bayarPiutang(int jumlah, String tgl, String faktur){
        String url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_BAYAR_PELUNASAN;
        //myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(),""+url);

        loading = ProgressDialog.show(Toko.this, "", "Mohon tunggu...", true);
        //bisa order
        JSONObject jsonBody;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(GlobalConfig.USER_ID, pref.getString(GlobalConfig.USER_ID, ""));
            jsonBody.put(GlobalConfig.USER_TOKEN, pref.getString(GlobalConfig.USER_TOKEN, ""));

            jsonBody.put(GlobalConfig.ID_TOKO, pref.getString(GlobalConfig.ID_TOKO, ""));
            jsonBody.put(GlobalConfig.UP_TGL_BAYAR, tgl);
            jsonBody.put(GlobalConfig.UP_JUMLAH_BAYAR, jumlah);
            jsonBody.put(GlobalConfig.UP_FAKTUR_BAYAR, faktur);
            myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), jsonBody.toString());
            request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loading.dismiss();
                    myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), response.toString());
                    try {
                        int status      = response.getInt("status");
                        String message  = response.getString("message");
                        if(status==1){
                            notifikasi_dialog("", message);
                            if(alert_detail_piutang != null){
                                myLog.print(getLocalClassName(), "dismiss");
                                alert_detail_piutang.dismiss();
                                alert_detail_piutang.cancel();
                            }
                        }else{
                            notifikasi(message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //myLog.print("respons",response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loading.dismiss();
                    NetworkResponse response = error.networkResponse;
                    if(response != null && response.data != null){
                        myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), "code"+response.statusCode);
                        switch(response.statusCode){
                            case 404:
                                notifikasi("Terjadi masalah dengan server.");
                                break;
                            case 408:
                                notifikasi("Waktu terlalu lama untuk memproses, silakan ulangi lagi!");
                                break;
                            case 500:
                                notifikasi("Terjadi masalah dengan server.");
                                break;
                            default:
                                notifikasi("Mohon maaf terjadi kesalahan.");
                                break;
                        }
                    }
                }
            }){
                public Map<String, String> getHeaders() {
                    Map<String,String> headers = new Hashtable<String, String>();

                    //Adding parameters
                    headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }};

            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MyAppController.getInstance().addToRequestQueue(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}