package com.muslimtekno.bigsales.config;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by agung on 09/03/2016.
 */
public class GlobalConfig {
    public static final String ENVIRONMENT      = "production";   //production  -> log tidak muncul, development -> log muncul
    public static final String APP_ID           = "Kdia83nds9u3msdkJds";       //key untuk menerima gcm
    public static final String APP_TOKEN        = "appToken";                //key untuk mengirim app
    public static final String NAMA_PREF        = "com.muslimtekno.semen.pref";  //key untuk menyimpan file preferences

    //192.168.43.208s
    //public static final String IP               = "http://192.168.43.208";          //alamat web/ip website
    public static final String IP               = "http://103.76.171.101";          //alamat web/ip server
    public static final String IP_KEY           = "ip";                     //key untuk alamat web/ip website
    public static final String WEB_URL          = "/apibig";                           //nama domain url


    public static final String TAG              = "mycop";                  //key untuk LOG

    public static final int MY_SOCKET_TIMEOUT_MS    = 30000;                    //30 detik
    public static final int MAX_STOK                = 10000000;                   //10 jt
    public static final int MAX_ROW_PER_REQUEST     = 25;                       //jumlah baris tiap request

    public static final int KODE_INTENT_FILTER_KUNJUNGAN    = 100;
    public static final int KODE_INTENT_FILTER_ORDER        = 101;
    public static final int KODE_INTENT_PILIH_TOKO          = 102;

    //permisiion untuk marshmallow pertama kali
    public static final String PERMISIION_CAMERA_FIRST_TIME     = "PERMISIION_CAMERA_FIRST_TIME";
    public static final String PERMISIION_LOCATION_FIRST_TIME   = "PERMISIION_LOCATION_FIRST_TIME";

    //URL
    public static final String URL_LOGIN            = "/login";
    public static final String URL_LOGOUT           = "/logout";
    public static final String URL_SCANTOKO         = "/scantoko";
    public static final String URL_SCANTOKO_TNP_BARCODE = "/scantokotanpabarcode";
    public static final String URL_TUTUPKUNJUNGAN   = "/tutupkunjungan";
    public static final String URL_INPUTSTOK        = "/inputstok";
    public static final String URL_FORMORDER        = "/cekfaktur";
    public static final String URL_FORMORDER_NON_SEMEN= "/cekfakturnonsemen";
    public static final String URL_FORMSTOK         = "/forminputstok";
    public static final String URL_SIMPANORDER      = "/simpanorder";
    public static final String URL_SIMPANORDER_NON_SEMEN    = "/simpanordernonsemen";
    public static final String URL_KEGIATAN         = "/updatekegiatan";

    public static final String URL_KUNJUNGANS       = "/kunjungans";
    public static final String URL_ORDERS           = "/orders";
    public static final String URL_DETAILORDER      = "/detailorder";

    public static final String URL_ORDERS_NONSEMEN  = "/orders_nonsemen";
    public static final String URL_DETAILORDER_NONSEMEN= "/detailorder_nonsemen";
    public static final String URL_TOKOS            = "/tokos";
    public static final String URL_TARGET           = "/target";
    public static final String URL_PIUTANGS         = "/piutangs";
    public static final String URL_DETAIL_PIUTANG   = "/detailpiutang";

    //untuk poin
    public static final String URL_POINS            = "/poin";
    public static final String URL_DETAIL_POIN      = "/detailpoin";
    public static final String URL_DETAIL_TUKAR     = "/detailtukar";

    public static final String URL_PEMBAYARANS      = "/pembayaran";
    public static final String URL_PELUNASAN_TOKO   = "/detail_pelunasan";
    public static final String URL_BAYAR_PELUNASAN  = "/bayar_pelunasan";

    //NOTIFIKASI
    public static final String notif_form_tidak_kosong  = "Username atau password tidak boleh kosong!";
    public static final String notif_butuh_koneksi      = "Aplikasi membutuhkan koneksi internet!";

    public static final String USER_NAME    = "user_name";
    public static final String USER_PASWORD = "user_password";
    public static final String IS_LOGIN     = "is_login";
    public static final String USER_ID      = "id_user";
    public static final String USER_TOKEN   = "user_token";

    //key untuk menyimpan target per user
    public static final String T_NAMA_PEGAWAI       = "NAMA_PEGAWAI";
    public static final String T_TOTAL_TARGET       = "TOTAL_TARGET";
    public static final String T_TOTAL_REALISASI    = "TOTAL_REALISASI";
    public static final String T_JUMLAH_ORDER       = "JUMLAH_ORDER";
    public static final String T_TOTAL_TRANSAKSI    = "TOTAL_TRANSAKSI";
    public static final String T_TAHUN              = "tahun";
    public static final String T_BULAN              = "bulan_string";

    public static final String IS_TOKO          = "IS_TOKO";
    public static final String ID_TOKO          = "KODE";
    public static final String GTOKO_NAMA       = "NAMA";
    public static final String GTOKO_ALAMAT     = "ALAMAT";
    public static final String GTOKO_PEMILIK    = "PIMPINAN";
    public static final String GREAL_PLAFON     = "REALPLAFON";
    public static final String GKDSALES         = "KDSALES";
    public static final String GJATUHTEMPOBAYAR = "JATUHTEMPOBAYAR";
    public static final String GTOKO_NOTELP     = "TELEPON";
    public static final String GTOKO_LAT        = "LATITUDE";
    public static final String GTOKO_LONG       = "LONGITUDE";
    public static final String GTOKO_NIK        = "NIK";
    public static final String GTOKO_POIN       = "POIN";
    public static final String GTOKO_PIUTANG_KAP        = "PIUTANG";
    public static final String GTOKO_TOTAL_PIUTANG_KAP  = "TOTAL_PIUTANG";
    public static final String GTOKO_REALPLAFON         = "REALPLAFON";
    public static final String GTOKO_TAGIHAN_TERDEKAT   = "TAGIHAN_TERDEKAT";
    public static final String GTOKO_TGL_JATUH_TEMPO    = "TGL_JATUH_TEMPO";
    public static final String GTOKO_TOTAL_PIUTANG      = "total_piutang";

    public static final String GKUNJUNGAN_TGL   = "TGL_KUNJUNGAN";
    public static final String GKUNJUNGAN_JAM_M = "JAM_MULAI";
    public static final String GSTOK_TOKO       = "STOK";

    public static final String TANGGAL_KIRIM    = "tanggal_kirim";
    public static final String JUMLAH_PESANAN   = "jumlah_pesanan";
    public static final String PRODUKID_SG01    = "SG01";
    public static final String PRODUKID_SG02    = "SG02";
    public static final String TOTAL_HARGA      = "total_harga";
    public static final String JENIS_PENGIRIMAN = "JENIS_PENGIRIMAN";
    public static final String IS_APPROVAL      = "is_approval";


    public static final String GPRODUK          = "PRODUK";
    public static final String GPRODUK_ID       = "ID_PRODUK";
    public static final String GPRODUK_NAMA     = "NAMA_PRODUK";


    public static final String GHARGA           = "HARGA";
    public static final String GPRODUK_NON_SEMEN= "produknonsemen";
    public static final String GGUDANG          = "GUDANG";
    public static final String GNAMA_GUDANG     = "NAMA";
    public static final String GID_GUDANG       = "KODE";
    public static final String GKODE_GUDANG     = "KODE_GUDANG";
    public static final String GDEFAULT_GUDANG  = "default_gudang";
    public static final String GADA_FAKTUR      = "ada_faktur";


    public static final String UP_START             = "START";
    public static final String UP_LIMIT             = "LIMIT";
    public static final String UP_KODE              = "KODE";
    public static final String UP_TGL_AWAL          = "TGL_AWAL";
    public static final String UP_TGL_AKHIR         = "TGL_AKHIR";

    public static final String UP_JUMLAH_BAYAR      = "jumlah_bayar";
    public static final String UP_TGL_BAYAR         = "tgl_bayar";
    public static final String UP_FAKTUR_BAYAR      = "faktur_bayar";

    public static final String GK_NIK               = "NIK";
    public static final String GK_NAMA_PEGAWAI      = "NAMA_PEGAWAI";
    public static final String GK_KODE              = "KODE";
    public static final String GK_NAMA_TOKO         = "NAMA_TOKO";
    public static final String GK_TGL_KUNJUNGAN     = "TGL_KUNJUNGAN";
    public static final String GK_JAM_MULAI         = "JAM_MULAI";
    public static final String GK_JAM_SELESAI       = "JAM_SELESAI";
    public static final String GK_DURASI            = "DURASI";

    public static final String GP_KODE              = "KODE";
    public static final String GP_NAMA_TOKO         = "NAMA";
    public static final String GP_ALAMAT            = "ALAMAT";
    public static final String GP_PIUTANG           = "PIUTANG";
    public static final String GP_JUMLAH_FAKTUR     = "JUMLAH_FAKTUR";
    public static final String GP_TGL_JATUH_TEMPO   = "TGL_JATUH_TEMPO";


    public static final String GDP_NO_FAKTUR        = "NO_FAKTUR";
    public static final String GDP_TGL_FAKTUR       = "TGL_FAKTUR";
    public static final String GDP_TOTAL            = "TOTAL";
    public static final String GDP_JATUH_TEMPO      = "TGL_JATUH_TEMPO";
    public static final String GDP_BELUM_DIBAYAR    = "PIUTANG";


    public static final String GO_TGL_FAKTUR        = "TGL_FAKTUR";
    public static final String GO_ID_ORDER          = "ID_ORDER";
    public static final String GO_NO_FAKTUR         = "NO_FAKTUR";
    public static final String GO_KODE              = "KODE";
    public static final String GO_NAMA_TOKO         = "NAMA_TOKO";
    public static final String GO_TOTAL             = "TOTAL";
    public static final String GO_TGL_JATUH_TEMPO   = "TGL_JATUH_TEMPO";
    public static final String GO_TGL_KIRIM         = "TGL_KIRIM";
    public static final String GO_NAMA_PEGAWAI      = "NAMA_PEGAWAI";
    public static final String GO_NAMA_GUDANG       = "NAMA_GUDANG";

    public static final String GT_KODE              = "KODE";
    public static final String GT_NAMA              = "NAMA";
    public static final String GT_ALAMAT            = "ALAMAT";

    public static final String GDO_NAMA_PRODUK      = "NAMA_PRODUK";
    public static final String GDO_JUMLAH           = "JUMLAH";
    public static final String GDO_HARGA            = "HARGA";
    public static final String GDO_JUMLAH_HARGA     = "JUMLAH_HARGA";

    public static final String FILTER_TOKOID        = "KODE";
    public static final String FILTER_TOKO_NAMA     = "NAMA";
    public static final String FILTER_TOKO_ALAMAT   = "ALAMAT";
    public static final String FILTER_TGL_MULAI     = "TGL_AWAL";
    public static final String FILTER_TGL_MULAI_S   = "TGL_AWAL_SHOW";
    public static final String FILTER_TGL_AKHIR     = "TGL_AKHIR";
    public static final String FILTER_TGL_AKHIR_S   = "TGL_AKHIR_SHOW";

    //untuk pembayaran
    public static final String PNAMA_PELANGGAN      = "NAMA_PELANGGAN";
    public static final String PALAMAT_PELANGGAN    = "ALAMAT_PELANGGAN";
    public static final String PPEMBAYARAN          = "PEMBAYARAN";
    public static final String PNO_KUITANSI         = "NO_KUITANSI";
    public static final String PTGL_KUITANSI        = "TGL_KUITANSI";
    public static final String PNO_FAKTUR           = "NO_FAKTUR";
    public static final String PTGL_FAKTUR          = "TGL_FAKTUR";
    public static final String PTOTAL               = "TOTAL";

    //untuk status pemesanan
    public static final Map<Integer, String> bulanToString;
    static
    {
        bulanToString = new HashMap<Integer, String>();
        bulanToString.put(1, "Januari");
        bulanToString.put(2, "Februari");
        bulanToString.put(3, "Maret");
        bulanToString.put(4, "April");
        bulanToString.put(5, "Mei");
        bulanToString.put(6, "Juni");
        bulanToString.put(7, "Juli");
        bulanToString.put(8, "Agustus");
        bulanToString.put(9, "September");
        bulanToString.put(10, "Oktober");
        bulanToString.put(11, "November");
        bulanToString.put(12, "Desember");

    };

    public static final String T_JENIS_ORDER_SEMEN  = "JENIS_ORDER_SEMEN";


}
