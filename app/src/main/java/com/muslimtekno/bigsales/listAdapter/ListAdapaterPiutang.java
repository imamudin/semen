package com.muslimtekno.bigsales.listAdapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.muslimtekno.bigsales.R;
import com.muslimtekno.bigsales.model.Piutang;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;

/**
 * Created by imamudin on 28/03/17.
 */
public class ListAdapaterPiutang extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Piutang> piutangs;

    public ListAdapaterPiutang(Activity activity, List<Piutang> piutangs) {
        this.activity = activity;
        this.piutangs = piutangs;
    }

    @Override
    public int getCount() {
        return piutangs.size();
    }

    @Override
    public Object getItem(int location) {
        return piutangs.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_piutang, null);

        TextView nama_toko      = (TextView) convertView.findViewById(R.id.tv_nama_toko);
        TextView alamat_toko    = (TextView) convertView.findViewById(R.id.tv_alamat_toko);
        TextView total_piutang  = (TextView) convertView.findViewById(R.id.tv_total_piutang);
        TextView jumlah_faktur  = (TextView) convertView.findViewById(R.id.tv_jumlah_faktur);
        TextView jatuh_tempo    = (TextView) convertView.findViewById(R.id.tv_jatuh_tempo);

        Piutang m = piutangs.get(position);

        nama_toko.setText(m.NAMA);
        alamat_toko.setText(m.ALAMAT);
        total_piutang.setText(decimalToRupiah(m.PIUTANG));
        jumlah_faktur.setText(m.JUMLAH_FAKTUR);
        jatuh_tempo.setText(m.TGL_JATUH_TEMPO);

        return convertView;
    }
    private String decimalToRupiah(double harga){
        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        kursIndonesia.setDecimalFormatSymbols(formatRp);

        String hasil = kursIndonesia.format(harga);
        return hasil.substring(0, (hasil.length()-3));
    }
}
