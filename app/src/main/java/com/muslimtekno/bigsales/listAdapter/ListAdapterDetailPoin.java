package com.muslimtekno.bigsales.listAdapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.muslimtekno.bigsales.R;
import com.muslimtekno.bigsales.model.DetailPoin;

import java.util.List;

/**
 * Created by imamudin on 02/04/17.
 */
public class ListAdapterDetailPoin extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<DetailPoin> poins;

    public ListAdapterDetailPoin(Activity activity, List<DetailPoin> poins) {
        this.activity = activity;
        this.poins = poins;
    }

    @Override
    public int getCount() {
        return poins.size();
    }

    @Override
    public Object getItem(int location) {
        return poins.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_detail_poin, null);

        TextView no_faktur      = (TextView) convertView.findViewById(R.id.tv_no_faktur);
        TextView jumlah_beli    = (TextView) convertView.findViewById(R.id.tv_jumlah_beli);
        TextView tgl_faktur     = (TextView) convertView.findViewById(R.id.tv_tgl_faktur);
        TextView total_poin     = (TextView) convertView.findViewById(R.id.tv_total_poin);

        DetailPoin m = poins.get(position);

        no_faktur.setText(m.NO_FAKTUR);
        jumlah_beli.setText(m.JUMLAH);
        total_poin.setText(m.POIN);
        tgl_faktur.setText(m.TGL_FAKTUR);

        return convertView;
    }
}