package com.muslimtekno.bigsales.listAdapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.muslimtekno.bigsales.R;
import com.muslimtekno.bigsales.app.MyAppController;
import com.muslimtekno.bigsales.model.Kunjungan;

import java.util.List;

/**
 * Created by imamudin on 12/03/17.
 */
public class ListAdapterKunjungan extends BaseAdapter{
    private Activity activity;
    private LayoutInflater inflater;
    private List<Kunjungan> kunjungenItems;
    ImageLoader imageLoader = MyAppController.getInstance().getImageLoader();

    public ListAdapterKunjungan(Activity activity, List<Kunjungan> kunjungenItems) {
        this.activity = activity;
        this.kunjungenItems = kunjungenItems;
    }

    @Override
    public int getCount() {
        return kunjungenItems.size();
    }

    @Override
    public Object getItem(int location) {
        return kunjungenItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_kunjungan, null);


        TextView nama_toko      = (TextView) convertView.findViewById(R.id.tv_nama_toko);
        TextView tgl_kunjungan  = (TextView) convertView.findViewById(R.id.tv_tgl_kunjungan);
        TextView durasi         = (TextView) convertView.findViewById(R.id.tv_durasi);
        TextView jam_mulai      = (TextView) convertView.findViewById(R.id.tv_jam_mulai);
        TextView jam_selesai    = (TextView) convertView.findViewById(R.id.tv_jam_selesai);

        Kunjungan m = kunjungenItems.get(position);

        nama_toko.setText(m.NAMA_TOKO);
        tgl_kunjungan.setText(m.TGL_KUNJUNGAN);
        durasi.setText(m.DURASI);
        jam_mulai.setText(m.JAM_MULAI);
        jam_selesai.setText(m.JAM_SELESAI);

        return convertView;
    }
}
