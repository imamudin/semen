package com.muslimtekno.bigsales.listAdapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.muslimtekno.bigsales.R;
import com.muslimtekno.bigsales.app.MyAppController;
import com.muslimtekno.bigsales.model.Order;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;

/**
 * Created by imamudin on 12/03/17.
 */
public class ListAdapterOrder extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Order> orders;
    ImageLoader imageLoader = MyAppController.getInstance().getImageLoader();

    public ListAdapterOrder(Activity activity, List<Order> orders) {
        this.activity = activity;
        this.orders = orders;
    }

    @Override
    public int getCount() {
        return orders.size();
    }

    @Override
    public Object getItem(int location) {
        return orders.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_order, null);


        TextView nama_toko      = (TextView) convertView.findViewById(R.id.tv_nama_toko);
        TextView tgl_faktur     = (TextView) convertView.findViewById(R.id.tv_tgl_order);
        TextView faktur         = (TextView) convertView.findViewById(R.id.tv_faktur);
        TextView nama_gudang    = (TextView) convertView.findViewById(R.id.tv_nama_gudang);
        TextView jatuh_tempo    = (TextView) convertView.findViewById(R.id.tv_jatuh_tempo);
        TextView tgl_kirim      = (TextView) convertView.findViewById(R.id.tv_tgl_kirim);
        TextView total          = (TextView) convertView.findViewById(R.id.tv_total);

        Order m = orders.get(position);

        nama_toko.setText(m.NAMA_TOKO);
        tgl_faktur.setText(m.TGL_FAKTUR);
        faktur.setText(m.NO_FAKTUR);
        nama_gudang.setText(m.NAMA_GUDANG);
        jatuh_tempo.setText(m.TGL_JATUH_TEMPO);
        tgl_kirim.setText(m.TGL_KIRIM);
        total.setText(decimalToRupiah(Double.parseDouble(m.TOTAL)));

        return convertView;
    }
    private String decimalToRupiah(double harga){
        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        kursIndonesia.setDecimalFormatSymbols(formatRp);

        String hasil = kursIndonesia.format(harga);
        return hasil.substring(0, (hasil.length()-3));
    }
}
