package com.muslimtekno.bigsales.listAdapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.muslimtekno.bigsales.model.Pembayaran;
import com.muslimtekno.bigsales.R;
import com.muslimtekno.bigsales.app.MyAppController;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;

/**
 * Created by imamudin on 02/04/17.
 */
public class ListAdapterPembayaran extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Pembayaran> pembayaran;
    ImageLoader imageLoader = MyAppController.getInstance().getImageLoader();

    public ListAdapterPembayaran(Activity activity, List<Pembayaran> pembayaran) {
        this.activity = activity;
        this.pembayaran = pembayaran;
    }

    @Override
    public int getCount() {
        return pembayaran.size();
    }

    @Override
    public Object getItem(int location) {
        return pembayaran.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_pembayaran, null);

        TextView nama_toko          = (TextView) convertView.findViewById(R.id.tv_nama_toko);
        TextView alamat_toko        = (TextView) convertView.findViewById(R.id.tv_alamat_toko);

        TextView jenis_pembayaran   = (TextView) convertView.findViewById(R.id.tv_pembayaran);
        TextView no_kuitansi        = (TextView) convertView.findViewById(R.id.tv_no_kuitansi);
        TextView tgl_kuitansi       = (TextView) convertView.findViewById(R.id.tv_tgl_kuitansi);
        TextView no_faktur          = (TextView) convertView.findViewById(R.id.tv_no_faktur);
        TextView tgl_faktur         = (TextView) convertView.findViewById(R.id.tv_tgl_faktur);
        TextView total              = (TextView) convertView.findViewById(R.id.tv_total_pembayaran);

        Pembayaran m = pembayaran.get(position);

        nama_toko.setText(m.NAMA_PELANGGAN);
        alamat_toko.setText(m.ALAMAT_PELANGGAN);
        jenis_pembayaran.setText(m.PEMBAYARAN);
        no_kuitansi.setText(m.NO_KUITANSI);
        tgl_kuitansi.setText(m.TGL_KUITANSI);
        no_faktur.setText(m.NO_FAKTUR);
        tgl_faktur.setText(m.TGL_FAKTUR);
        total.setText(decimalToRupiah(Double.parseDouble(m.TOTAL)));

        return convertView;
    }
    private String decimalToRupiah(double harga){
        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        kursIndonesia.setDecimalFormatSymbols(formatRp);

        String hasil = kursIndonesia.format(harga);
        return hasil.substring(0, (hasil.length()-3));
    }
}