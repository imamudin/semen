package com.muslimtekno.bigsales.listAdapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.muslimtekno.bigsales.R;
import com.muslimtekno.bigsales.app.MyAppController;
import com.muslimtekno.bigsales.model.Poin;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;

/**
 * Created by imamudin on 02/04/17.
 */
public class ListAdapterPoin extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Poin> poins;
    ImageLoader imageLoader = MyAppController.getInstance().getImageLoader();

    public ListAdapterPoin(Activity activity, List<Poin> poins) {
        this.activity = activity;
        this.poins = poins;
    }

    @Override
    public int getCount() {
        return poins.size();
    }

    @Override
    public Object getItem(int location) {
        return poins.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_poin, null);

        TextView tv_nama_toko    = (TextView) convertView.findViewById(R.id.tv_nama_toko);
        TextView tv_total_poin   = (TextView) convertView.findViewById(R.id.tv_total_poin);
        TextView tv_poin         = (TextView) convertView.findViewById(R.id.tv_poin);
        TextView tv_alamat       = (TextView) convertView.findViewById(R.id.tv_alamat);
        TextView tv_jumlah_order = (TextView) convertView.findViewById(R.id.tv_jumlah_order);
        TextView tv_jumlah_beli  = (TextView) convertView.findViewById(R.id.tv_jumlah_beli);
        TextView tv_tukar        = (TextView) convertView.findViewById(R.id.tv_tukar);

        Poin poin = poins.get(position);

        tv_nama_toko.setText(poin.NAMA);
        tv_total_poin.setText(poin.TOTAL_POIN);
        tv_poin.setText(poin.POIN);
        tv_alamat.setText(poin.ALAMAT);
        tv_jumlah_order.setText(poin.JUMLAH_ORDER+" kali");
        tv_jumlah_beli.setText(poin.JUMLAH_BELI+ " sak");
        tv_tukar.setText(poin.TUKAR);

        return convertView;
    }
    private String decimalToRupiah(double harga){
        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        kursIndonesia.setDecimalFormatSymbols(formatRp);

        String hasil = kursIndonesia.format(harga);
        return hasil.substring(0, (hasil.length()-3));
    }
}
