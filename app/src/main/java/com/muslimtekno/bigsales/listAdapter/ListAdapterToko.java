package com.muslimtekno.bigsales.listAdapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.muslimtekno.bigsales.R;
import com.muslimtekno.bigsales.app.MyAppController;
import com.muslimtekno.bigsales.model.TokoModel;

import java.util.List;

/**
 * Created by imamudin on 14/03/17.
 */
public class ListAdapterToko extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<TokoModel> tokoModels;
    ImageLoader imageLoader = MyAppController.getInstance().getImageLoader();

    public ListAdapterToko(Activity activity, List<TokoModel> tokoModels) {
        this.activity = activity;
        this.tokoModels = tokoModels;
    }

    @Override
    public int getCount() {
        return tokoModels.size();
    }

    @Override
    public Object getItem(int location) {
        return tokoModels.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_toko, null);


        TextView nama_toko  = (TextView) convertView.findViewById(R.id.tv_nama_toko);
        TextView alamat     = (TextView) convertView.findViewById(R.id.tv_alamat_toko);

        TokoModel m = tokoModels.get(position);

        nama_toko.setText(m.NAMA);
        alamat.setText(m.ALAMAT);

        return convertView;
    }
}
