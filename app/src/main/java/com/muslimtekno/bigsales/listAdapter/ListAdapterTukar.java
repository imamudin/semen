package com.muslimtekno.bigsales.listAdapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.muslimtekno.bigsales.R;
import com.muslimtekno.bigsales.model.DetailTukar;

import java.util.List;

/**
 * Created by imamudin on 02/04/17.
 */
public class ListAdapterTukar extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<DetailTukar> poins;

    public ListAdapterTukar(Activity activity, List<DetailTukar> poins) {
        this.activity = activity;
        this.poins = poins;
    }

    @Override
    public int getCount() {
        return poins.size();
    }

    @Override
    public Object getItem(int location) {
        return poins.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_tukar, null);

        TextView reward         = (TextView) convertView.findViewById(R.id.tv_reward);
        TextView poin           = (TextView) convertView.findViewById(R.id.tv_total_poin);
        TextView tgl_tukar      = (TextView) convertView.findViewById(R.id.tv_tgl_tukar);

        DetailTukar m = poins.get(position);

        reward.setText(m.REWARD);
        poin.setText(m.POIN);
        tgl_tukar.setText(m.TGL_TUKAR);

        return convertView;
    }
}
