package com.muslimtekno.bigsales.model;

import android.util.Log;

import org.json.JSONObject;

/**
 * Created by imamudin on 02/04/17.
 */
public class DetailPoin {
    public String dataJson;
    public String NAMA, ALAMAT, NO_FAKTUR, TGL_FAKTUR, JUMLAH, POIN;

    public  DetailPoin(){}
    public  DetailPoin(String objectPoin){
        this.dataJson   = objectPoin;
        try{
            JSONObject poin = new JSONObject(objectPoin);

            this.NAMA           = poin.getString("NAMA");
            this.ALAMAT         = poin.getString("ALAMAT");
            this.NO_FAKTUR      = poin.getString("NO_FAKTUR");
            this.TGL_FAKTUR     = poin.getString("TGL_FAKTUR");
            this.JUMLAH         = poin.getString("JUMLAH");
            this.POIN           = poin.getString("POIN");
        }catch (Exception e){
            Log.d(getClass().toString(),"errot memproses data object.");
        }

    }
}
