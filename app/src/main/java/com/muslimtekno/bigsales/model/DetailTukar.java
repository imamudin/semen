package com.muslimtekno.bigsales.model;

import android.util.Log;

import org.json.JSONObject;

/**
 * Created by imamudin on 02/04/17.
 */
public class DetailTukar {
    public String dataJson;
    public String NAMA, ALAMAT, TGL_TUKAR, POIN, REWARD;

    public  DetailTukar(){}
    public  DetailTukar(String objectPoin){
        this.dataJson   = objectPoin;
        try{
            JSONObject poin = new JSONObject(objectPoin);

            this.NAMA           = poin.getString("NAMA");
            this.ALAMAT         = poin.getString("ALAMAT");
            this.TGL_TUKAR      = poin.getString("TGL_TUKAR");
            this.POIN           = poin.getString("POIN");
            this.REWARD         = poin.getString("REWARD");
        }catch (Exception e){
            Log.d(getClass().toString(),"error memproses data object.");
        }
    }
}