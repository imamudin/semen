package com.muslimtekno.bigsales.model;

/**
 * Created by imamudin on 12/03/17.
 */
public class Kunjungan {
    public String NIK,NAMA_PEGAWAI,KODE,NAMA_TOKO,TGL_KUNJUNGAN,JAM_MULAI,JAM_SELESAI,DURASI;

    public Kunjungan(String NAMA_PEGAWAI, String NIK, String KODE, String NAMA_TOKO, String TGL_KUNJUNGAN, String JAM_MULAI, String JAM_SELESAI, String DURASI) {
        this.NAMA_PEGAWAI = NAMA_PEGAWAI;
        this.NIK = NIK;
        this.KODE = KODE;
        this.NAMA_TOKO = NAMA_TOKO;
        this.TGL_KUNJUNGAN = TGL_KUNJUNGAN;
        this.JAM_MULAI = JAM_MULAI;
        this.JAM_SELESAI = JAM_SELESAI;
        this.DURASI = DURASI;
    }
}
