package com.muslimtekno.bigsales.model;

/**
 * Created by imamudin on 12/03/17.
 */
public class Order {
    public String TGL_FAKTUR, ID_ORDER,NO_FAKTUR,KODE,NAMA_TOKO,TOTAL,TGL_JATUH_TEMPO,TGL_KIRIM,
    NAMA_PEGAWAI,NAMA_GUDANG;

    public Order(String TGL_FAKTUR, String ID_ORDER, String NO_FAKTUR, String KODE, String NAMA_TOKO, String TOTAL, String TGL_JATUH_TEMPO, String TGL_KIRIM, String NAMA_PEGAWAI, String NAMA_GUDANG) {
        this.TGL_FAKTUR = TGL_FAKTUR;
        this.ID_ORDER = ID_ORDER;
        this.NO_FAKTUR = NO_FAKTUR;
        this.KODE = KODE;
        this.NAMA_TOKO = NAMA_TOKO;
        this.TOTAL = TOTAL;
        this.TGL_JATUH_TEMPO = TGL_JATUH_TEMPO;
        this.TGL_KIRIM = TGL_KIRIM;
        this.NAMA_PEGAWAI = NAMA_PEGAWAI;
        this.NAMA_GUDANG = NAMA_GUDANG;
    }
}
