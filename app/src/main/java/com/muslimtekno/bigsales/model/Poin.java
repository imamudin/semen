package com.muslimtekno.bigsales.model;

import android.util.Log;

import org.json.JSONObject;

/**
 * Created by imamudin on 02/04/17.
 */
public class Poin {
    public String dataJson;
    public String KODE, NAMA, ALAMAT, JUMLAH_ORDER, JUMLAH_BELI, TOTAL_POIN, TUKAR, POIN;
    public Poin(){}
    public Poin(String objectPoin){
        this.dataJson   = objectPoin;
        try{
            JSONObject poin = new JSONObject(objectPoin);

            this.KODE           = poin.getString("KODE");
            this.NAMA           = poin.getString("NAMA");
            this.ALAMAT         = poin.getString("ALAMAT");
            this.JUMLAH_ORDER   = poin.getString("JUMLAH_ORDER");
            this.JUMLAH_BELI    = poin.getString("JUMLAH_BELI");
            this.TOTAL_POIN     = poin.getString("TOTAL_POIN");
            this.TUKAR          = poin.getString("TUKAR");
            this.POIN           = poin.getString("POIN");
        }catch (Exception e){
            Log.d(getClass().toString(),"errot memproses data object.");
        }

    }
}
