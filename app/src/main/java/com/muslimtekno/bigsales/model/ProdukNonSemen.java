package com.muslimtekno.bigsales.model;

import android.util.Log;

import org.json.JSONObject;

/**
 * Created by imamudin on 25/03/17.
 */
public class ProdukNonSemen {
    public String NAMA_PRODUK="", SATUAN="";
    public int  ID_PRODUK=0;
    public int HARGA=0;

    public ProdukNonSemen(String s_produk){
        try{
            JSONObject poin = new JSONObject(s_produk);

            this.ID_PRODUK      = poin.getInt("ID_PRODUK");
            this.NAMA_PRODUK    = poin.getString("NAMA_PRODUK");
            this.SATUAN         = poin.getString("SATUAN");
            this.HARGA          = poin.getInt("HARGA");
        }catch (Exception e){
            Log.d(getClass().toString(),"error memproses data object.");
        }
    }
    @Override
    public String toString() {
        return NAMA_PRODUK;
    }
}
