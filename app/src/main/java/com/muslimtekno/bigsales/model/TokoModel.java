package com.muslimtekno.bigsales.model;

/**
 * Created by imamudin on 12/03/17.
 */
public class TokoModel {
    public String KODE, NAMA, ALAMAT;

    public TokoModel(String KODE, String NAMA, String ALAMAT) {
        this.KODE = KODE;
        this.NAMA = NAMA;
        this.ALAMAT = ALAMAT;
    }
}
